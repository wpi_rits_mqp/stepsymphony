# Step Symphony #

Step Symphony is a location-based mobile game in which players can search for run-away

creatures throughout Central Kyoto. These creatures, known as kyokubou (short for kyoku no

bouame, or Musical Candy), are colorful animals in Japanese attire that play instruments. In

order to find and catch these kyokubou, players must utilize panning directional music as well as

their phone’s camera to locate them. While searching for the kyokubou, the player is

accompanied by the monkey kyokubou Sarushi. When the player finds the kyokubou, they are

then prompted to capture the kyokubou by playing a rhythm game. When the kyokubou has 

been captured, they are added to the player's collection and their instruments can be 

played. You can also view a story about the relationship of each kyokubou and their 

location within central Kyoto.
### Version 1.01 ###


### Who do I talk to? ###

* Marco Duran (mdduran@outlook.com)
* LilyAnne Lewis (llewis5678@gmail.com)
* Sienna McDowell