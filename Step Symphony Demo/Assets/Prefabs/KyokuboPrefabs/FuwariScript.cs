﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class FuwariScript : Kyokubou {
    public static FuwariScript fuwari;

    public FuwariScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Fuwari";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
    }
}
