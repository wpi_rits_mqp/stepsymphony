﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class KunKunScript : Kyokubou {
    public static KunKunScript kun_kun;

    public KunKunScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Kun-Kun";
        instrument = "Shamisen";
        image = "KyokuboSprites/" + kyoname;
    }
	
}
