﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class OsachiScript : Kyokubou {
    public static OsachiScript osachi;
    public OsachiScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Osachi";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
    }
}
