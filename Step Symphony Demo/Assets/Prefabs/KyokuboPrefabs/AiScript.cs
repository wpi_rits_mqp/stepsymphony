﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class AiScript : Kyokubou {
    public static AiScript ai;
	public AiScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Ai";
        instrument = "Shamisen";
        image = "KyokuboSprites/" + kyoname;
        
    }
}
