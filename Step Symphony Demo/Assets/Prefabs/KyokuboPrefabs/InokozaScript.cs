﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class InokozaScript : Kyokubou {
    public static InokozaScript inokoza;
    public InokozaScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Inokoza";
        instrument = "Shamisen";
        image = "KyokuboSprites/" + kyoname;
    }
}
