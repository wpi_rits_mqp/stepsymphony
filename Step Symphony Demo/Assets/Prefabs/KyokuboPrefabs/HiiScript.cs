﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class HiiScript : Kyokubou {
    public static HiiScript hii;
    public HiiScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Hii";
        instrument = "Shamisen";
        image = "KyokuboSprites/" + kyoname;
    }
}
