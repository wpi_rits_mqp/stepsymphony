﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class KataakiScript : Kyokubou {
    public static KataakiScript kataaki;
    public KataakiScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Kataaki";
        instrument = "Shamisen";
        image = "KyokuboSprites/" + kyoname;
    }
}
