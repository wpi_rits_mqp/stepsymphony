﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ShoujiScript : Kyokubou {
    public static ShoujiScript shouji;
    public ShoujiScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Shouji";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
    }
}
