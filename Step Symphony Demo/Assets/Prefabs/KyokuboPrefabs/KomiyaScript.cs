﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class KomiyaScript : Kyokubou {
    public static KomiyaScript komiya;
    public KomiyaScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Komiya";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
    }
}
