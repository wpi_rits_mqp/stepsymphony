﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class KumagamineScript : Kyokubou {
    public static KumagamineScript kumagamine;
    public KumagamineScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Kumagamine";
        instrument = "Shamisen";
    }
}
