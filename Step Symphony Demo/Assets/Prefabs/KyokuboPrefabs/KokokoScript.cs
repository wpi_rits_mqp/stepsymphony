﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class KokokoScript : Kyokubou {
    public static KokokoScript kokoko;
    public KokokoScript()
    {
        location_one = new Location("Library", 42.27430, -71.80670);
        location_two = new Location();
        location_three = new Location();
        kyoname = "Kokoko";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
        reward = "Audio/ShinobueCollections/Shinobue F5.wav";
        music = "Audio/CollectionMusic/ss_song_01.wav";//Need to make song
        description = "As a child, Kokoko loved going to the Minamiza Kabuki Theatre.  Her mother would take her every weekend to watch her father, Kataaki, play.  Now, Kokoko spends much of her time performing on stage, as well as playing the Shinobue with her father on the streets of Gion.";
    }
}
