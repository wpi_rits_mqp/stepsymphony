﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class TomaruScript : Kyokubou {
    public static TomaruScript tomaru;
    public TomaruScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Tomaru";
        instrument = "Shamisen";
        image = "KyokuboSprites/" + kyoname;
    }
}
