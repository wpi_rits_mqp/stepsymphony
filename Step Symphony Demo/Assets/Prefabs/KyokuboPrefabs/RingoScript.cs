﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class RingoScript : Kyokubou {
    public static RingoScript ringo;
    public RingoScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Ringo";
        instrument = "Shamisen";
        image = "KyokuboSprites/" + kyoname;
    }
}
