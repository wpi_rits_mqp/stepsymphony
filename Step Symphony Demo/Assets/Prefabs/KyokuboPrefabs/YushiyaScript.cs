﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class YushiyaScript : Kyokubou {
    public static YushiyaScript yushiya;
    public YushiyaScript()
    {
        location_one = new Location("CampusCenter", 42.24467, -71.80858);
        location_two = new Location();
        location_three = new Location();
        kyoname = "Yushiya";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
        reward = "Audio/ShinobueCollections/Shinobue_C#5.wav";
        music = "Audio/CollectionMusic/ss_song_01.wav";//Need to make song
        description = "Whenever Ai has questions about the latest fashion trends, she always goes to Yushiya for help.  Yushiya works at a clothing store in Kyoto Station, and is always excited to talk about new makeup and mail polish.  If you need advice on coordinating outfits, or just want company going to the spa, then never hesitate to ask Yushiya.  She will never say no.";
    }
}
