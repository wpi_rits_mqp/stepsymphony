﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class UshikyuuScript : Kyokubou {
    public static UshikyuuScript ushikyuu;
    public UshikyuuScript()
    {
        location_one = new Location();
        location_two = new Location();
        location_three = new Location();
        kyoname = "Ushikyuu";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
    }
}
