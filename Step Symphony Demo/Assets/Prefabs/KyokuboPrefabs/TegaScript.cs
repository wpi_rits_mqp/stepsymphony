﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class TegaScript : Kyokubou {
    public static TegaScript tega;
    public TegaScript()
    {
        location_one = new Location("Quad", 42.27387, -71.80999);
        location_two = new Location();
        location_three = new Location();
        kyoname = "Tega";
        instrument = "Shinobue";
        image = "KyokuboSprites/" + kyoname;
        reward = "Audio/ShinobueCollections/Shinobue A5.wav";
        music = "Audio/CollectionMusic/ss_song_01.wav";//Need to make song
        description = "If you know what's good for you, never mention the Kyoto Aquarium in front of Tega.  Every day, he tries to escape.  Every time, he's always caught in either Umekoji Park or Shosei-en Garden and brought back.  Tega doesn't want to be there, he wants to spend time with Osachi.  So don't send him back to the Aquarium.  He'll never forgive you.";
    }
}
