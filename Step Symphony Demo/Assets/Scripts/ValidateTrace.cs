﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ValidateTrace : MonoBehaviour
{
    //Game Objects that represent trace points
    public GameObject tracePoint1;
    public GameObject tracePoint2;
    public GameObject tracePoint3;

    public GameObject[] tracePoints;

    //Colliders for all of the trace points
    private CircleCollider2D tpCollider1;
    private CircleCollider2D tpCollider2;
    private CircleCollider2D tpCollider3;

    //Booleans for validation
    public bool hitPoint1;
    public bool hitPoint2;
    public bool hitPoint3;

    public bool tp1Hit;
    public bool tp2Hit;
    public bool tp3Hit;

    public bool traceInOrder;

    //Validation score, 3 = capture success: -2 = capture failure
    public int score;

    //Capture Script
    CaptureProgress captureScript;
    TracePointBehavior tracePointScript;

    //Line Renderer Script
    DrawLine lineScript;

    //Place where the finger is touching the screen
    Vector3 touchPos;
    Vector2 touchPos2D;

    //Raycast hit
    RaycastHit2D[] touchInfo;

    //Canvas as a game object
    public GameObject canvas;

    // Use this for initialization
    void Start()
    {
        canvas = GameObject.Find("Canvas");
        if(canvas != null)
        {
            captureScript = canvas.GetComponent<CaptureProgress>();
        }
        //Initialize capture script from the canvas
        

        //Initialize tracepoint script
        tracePointScript = GetComponentInChildren<TracePointBehavior>();

        lineScript = GameObject.FindGameObjectWithTag("LineRenderer").GetComponent<DrawLine>();
        //Get score from a separate script to determine capture success
        score = 0;
        //Initialize tracepoints from children
        //tracePoint1 = GameObject.FindGameObjectWithTag("TracePoint1");
        //tracePoint2 = GameObject.FindGameObjectWithTag("TracePoint2");
        //tracePoint3 = GameObject.FindGameObjectWithTag("TracePoint3");

        tracePoints = GameObject.FindGameObjectsWithTag("TracePoint");
        for(int i = 0; i < tracePoints.Length; i++)
        {
            if(tracePoints[i].GetComponent<TracePointBehavior>().orderNum == 1)
            {
                tracePoint1 = tracePoints[i];
            }
            else if (tracePoints[i].GetComponent<TracePointBehavior>().orderNum == 2)
            {
                tracePoint2 = tracePoints[i];
            }
            else if (tracePoints[i].GetComponent<TracePointBehavior>().orderNum == 3)
            {
                tracePoint3 = tracePoints[i];
            }
        }
        

        //Find the circle colliders of the trace points if they exist
        if (tracePoint1 != null)
            tpCollider1 = tracePoint1.GetComponent<CircleCollider2D>();

        if (tracePoint2 != null)
            tpCollider2 = tracePoint2.GetComponent<CircleCollider2D>();

        if (tracePoint3 != null)
            tpCollider3 = tracePoint3.GetComponent<CircleCollider2D>();

        //Initialize booleans
        hitPoint1 = false;
        hitPoint2 = false;
        hitPoint3 = false;
        traceInOrder = false;
    }

    /*public void OnDrag(PointerEventData finger)
    {
        TraceFirst(finger.position);
        if (hitPoint1)
        {
            TraceSecond(finger.position);

            if (hitPoint1 && hitPoint2)
            {
                TraceThird(finger.position);

                if (hitPoint1 && hitPoint2 && hitPoint3)
                {
                    traceInOrder = true;
                }
            }
        }
    }*/

    /*public void OnEndDrag(PointerEventData finger)
    {
        if (traceInOrder)
        {
            //add to the score 
            score++;
            UpdateScore(score);
            ResetHits();
        }
        else
        {
            score--;
            UpdateScore(score);
            ResetHits();
        }
    } */

    // Update is called once per frame
    void FixedUpdate()
    {
        tp1Hit = tracePoint1.GetComponent<TracePointBehavior>().isHit;
        tp2Hit = tracePoint2.GetComponent<TracePointBehavior>().isHit;
        tp3Hit = tracePoint3.GetComponent<TracePointBehavior>().isHit;

        TraceFirst();
        TraceSecond();
        TraceThird();

        //Check to see if the player is touching the screen
        if (Input.touchCount > 0 && Input.touchCount < 2)
        {
            //Get where the player is touching the screen
            touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            //Convert to 2d for 2d raycasting
            touchPos2D = new Vector2(touchPos.x, touchPos.y);
            //Set the raycast hit to where the player is touching the screen
            touchInfo = Physics2D.RaycastAll(touchPos2D, Camera.main.transform.forward);

            //check if all three points are hit
            if(Input.GetTouch(0).phase == TouchPhase.Began)
            {
                traceInOrder = false;
                ResetHits();
            }
            if(Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                if (traceInOrder)
                {
                    score++;
                    UpdateScore(score);
                    ResetHits();
                }
                else if (!traceInOrder)
                {
                    score--;
                    UpdateScore(score);
                    ResetHits();
                }
            }
        }
        
            
        
    }

    //void resetHits, sets all trace hits to 0
    void ResetHits()
    {
        for(int i = 0; i < tracePoints.Length; i++)
        {
            tracePoints[i].GetComponent<TracePointBehavior>().isHit = false;
        }
        hitPoint1 = false;
        hitPoint2 = false;
        hitPoint3 = false;
    }
    //void Update Score
    //Updates the score from this one and sets it in the other script
    void UpdateScore(int score)
    {
        //set the score value in other script to the one in this script
        captureScript.captureScore = score;
    }


    //boolean to check if the first point is hit before the next 2 points
    void TraceFirst()
    {
        if (tp1Hit && !tp2Hit && !tp3Hit && !hitPoint2 && !hitPoint3)
        {
            hitPoint1 = true;
        }
        
    }
    //boolean to check if the second point is hit after the first point is hit and if the 3 one is not hit
    void TraceSecond()
    {
        if (tp2Hit && tp1Hit && !tp3Hit && hitPoint1 && !hitPoint3)
            {
                hitPoint2 = true;

            }
        
    }
    //boolean to check if the last point is hit after the other 2 are hit
    void TraceThird()
    {
        if (hitPoint2 && hitPoint1 && tp3Hit)
        {
                hitPoint3 = true;
                traceInOrder = true;
        }
        
    }
}
