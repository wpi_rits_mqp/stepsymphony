﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class LogIn : MonoBehaviour {
    public StepSymphonyBackend ssBackend;
    //public GameObject player;
    bool isLoggingIn = false;
    string loginStatus = "";
    public string username = "", password = "";
    public bool rememberUser = false;


    public GameObject userBox, passBox, player;
    private InputField userText, passText;
    private Text statusText;
    //add checkbox for remmebering credentials

    private LoadScene loadingScreenScript;

    //private PlayerData playerData;
    SaveGame databaseSave;

	// Use this for initialization
	void Awake () {
        if(player == null)
            player = GameObject.FindGameObjectWithTag("Player");
        if (ssBackend == null && player != null)
        {
            ssBackend = player.GetComponent<PlayerData>().ssBackend;
        }
        else if(ssBackend != null)
            ssBackend = PlayerData.playerData.ssBackend;
        userBox = GameObject.Find("UserInput");
        userText = userBox.GetComponent<InputField>();
        passBox = GameObject.Find("PasswordInput");
        passText = passBox.GetComponent<InputField>();
        loadingScreenScript = GetComponent<LoadScene>();

        statusText = GameObject.Find("StatusText").GetComponent<Text>();
        if(ssBackend != null){
            ssBackend.OnLoggedIn += OnLoggedIn;
            ssBackend.OnLoginFail += OnLoginFailed;
            //check to see if there is a savefile on disk
            //else we will add a delegate for loading
            ssBackend.OnGameLoaded += OnGameLoaded;
            ssBackend.OnGameLoadedFail += OnGameLoadedFail;
        }
	}
	
    void OnLoggedIn()
    {
        loginStatus = "Login Successful!";
        isLoggingIn = false;

        if (rememberUser)
            SaveInfo();
        else
            RemoveInfo();

        //set player data info now
        PlayerData.playerData.username = username;

        //check here to see if the player has a save file locally
        //if(File.Exists(Application.persistentDataPath + "/save.json"))
        //{
        //update the save
        //get the savegame from the database
        //ssBackend.LoadGame();
        //add the info to the player data
        //playerData.PopulatePlayerData(databaseSave);
        //}
        //else
        //{
        //***********Go To Loading Screen For Map Here**********************
        //loadingScreenScript.OnClick();
        loadingScreenScript.Load();
        Debug.Log("Login Successful");
        //}
        
        
    }

    /// <summary>
    /// This will take the username and save it to the player prefs
    /// Strings to be saved will be savedUser, and savedPass
    /// </summary>
    void SaveInfo()
    {
        PlayerPrefs.SetString("savedUser", username.ToBase64());
        PlayerPrefs.SetString("savedPass", password.ToBase64());
    }

    /// <summary>
    /// This will not save any of the user info to the player prefs
    /// If there was anything there, set the strings to null values on a successful login
    /// </summary>
    void RemoveInfo()
    {
        if (PlayerPrefs.HasKey("savedPass"))
        {
            PlayerPrefs.SetString("savedUser", "");
            PlayerPrefs.SetString("savedPass", "");
        }
    }

    void OnLoginFailed(string errMsg)
    {
        loginStatus = "Error! " + errMsg;
        isLoggingIn = false;
    }

    public void StartLogin()
    {
        if (isLoggingIn)
        {
            Debug.Log("User is already logging in.");
            return;
        }

        isLoggingIn = true;
        ssBackend.Login(username, password);
    }

    void OnGameLoaded(string jsonData)
    {
        //take the json data and deserialize it via the player data class
        databaseSave = PlayerData.playerData.LoadProgress(jsonData);
        //populate the player data
        PlayerData.playerData.PopulatePlayerData(databaseSave);
        PlayerData.playerData.jsonData = jsonData;
        //***********Go To Loading Screen For Map Here**********************
        loadingScreenScript.scene = "LoadingScreen";
        loadingScreenScript.asyncLoadingScene = "MapScreen";
        loadingScreenScript.Load();
        Debug.Log("Login Successful and DataLoaded from database");
    }

    void OnGameLoadedFail(string errMsg)
    {
        //go back to the login screen with the status message
        loginStatus = errMsg;
        isLoggingIn = false;


    }

	// Update is called once per frame
	void Update () {
        //update the input boxes with the strings
        if (userText != null)
            username = userText.text;
        if (passText != null)
            password = passText.text;

        if (isLoggingIn)
        {
            statusText.text = "Login Status: ";
            statusText.text = statusText.text + " " + loginStatus;
        }
	}
}
