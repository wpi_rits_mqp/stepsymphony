﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SignUp : MonoBehaviour {
    private StepSymphonyBackend ssBackend;
    bool isSigningUp = false;
    string signUpStatus = "";
    public string username = "", email = "", password = "", confirmPassword = "";
    //Game objects that represent the boxes in the scene
    public GameObject emailBox, usernameBox, passBox, passConfirmBox;
    private InputField emailText, usernameText, passText, passConfirmText;
    public Text statusText;
    float statusChangeNum;
    LoadScene loadingScreenScript;

	// Use this for initialization
	void Start () {
        ssBackend = GetComponent<StepSymphonyBackend>();
        emailBox = GameObject.Find("EmailInput");
        emailText = emailBox.GetComponent<InputField>();
        usernameBox = GameObject.Find("UserInput");
        usernameText = usernameBox.GetComponent<InputField>();
        passBox = GameObject.Find("PasswordInput");
        passText = passBox.GetComponent<InputField>();
        passConfirmBox = GameObject.Find("PasswordConfirmInput");
        passConfirmText = passConfirmBox.GetComponent<InputField>();

        statusText = GameObject.Find("StatusText").GetComponent<Text>();
        loadingScreenScript = GetComponent<LoadScene>();

        if (ssBackend != null)
        {
            ssBackend.OnSignupSuccess += OnSignupSuccess;
            ssBackend.OnSignupFail += OnSignupFail;
        }
	}

    //when the signup is successful, change the status of the signup process
    void OnSignupSuccess()
    {
        statusText.text = "";
        signUpStatus = "Signup was successful!";
        statusText.text = "Signup Status: ";
        statusText.text = statusText.text + " " + signUpStatus;
        isSigningUp = false;
        Invoke("FinishSignup", 1.5f);
    }

    void FinishSignup()
    {
        //Go to the loading screen that loads the map
        Debug.Log("GOTO LOADING MAP HERE!");
        //log the user into the game, the delegate should handle calling the loading screen here...source code is in LogIn.cs
        ssBackend.Login(username, password);
        loadingScreenScript.scene = "LoadingScreen";
        loadingScreenScript.asyncLoadingScene = "MapScreen";
        loadingScreenScript.Load();
    }


    public void DoSignup()
    {
        signUpStatus = "Signing Up...";
        if (isSigningUp)
        {
            
            Debug.LogWarning("The player is already signing up, returning...");
            return;
        }
        //first check to see if the email and confirmed emails match
        if (email.Contains("@") && email.Contains(".com"))
        {
            //then check to see if the password and the confirmed passwords match
            if (password.Equals(confirmPassword))
            {
                //ssBackend.Signup(confirmEmail, confirmPassword);
                ssBackend.Signup(username, email, confirmPassword);
                isSigningUp = true;
            }
            else if (!password.Equals(confirmPassword))
            {
                Debug.LogWarning("Password and confirmed password are not the same, try again");
                return;
            }
        }
        else
        {
            Debug.LogWarning("Email is not a valid way to write an email address.");
            return;
        }


        
    }

    //when the signup is unsuccessful, change the status of the signup process
    void OnSignupFail(string errMsg)
    {
        signUpStatus = "Signup error: \t" + errMsg;
        statusText.text = "Signup Status: ";
        statusText.text = statusText.text + " " + signUpStatus;
        Debug.Log("SignUp failed");
        isSigningUp = false;
    }

    //functions to check if the email is a valid email string

    //check to see if the password is of a certain character length

    
	
	// Update is called once per frame
	void Update () {
        //always update the signup status
        if (emailText != null)
            email = emailText.text;
        if(usernameText != null)
            username = usernameText.text;
        if(passText != null)
            password = passText.text;
        if(passConfirmText != null)
            confirmPassword = passConfirmText.text;

        if (isSigningUp)
        {
            if(Time.time > statusChangeNum)
            {
                statusChangeNum = Time.time + 0.5f;
                statusText.text = "Signup Status: ";
                statusText.text = statusText.text + " " + signUpStatus;
            }
            
        }
	}
}
