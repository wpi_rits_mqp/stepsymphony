﻿// Copyright (c) 2015 Eamon Woortman
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// Modified by: Marco Duran to incorperate the use of storing locations and updating user progress

#define UNITY_4_PLUS
#define UNITY_5_PLUS

#if UNITY_4_0 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 || UNITY_4_4 || UNITY_4_5 || UNITY_4_6 || UNITY_4_7 || UNITY_4_8 || UNITY_4_9
#define UNITY_4_X
#undef UNITY_5_PLUS
#elif UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4 || UNITY_5_5 || UNITY_5_6 || UNITY_5_7 || UNITY_5_8 || UNITY_5_9
#define UNITY_5_X
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Newtonsoft.Json.Linq;

public enum Response
{
  ClientError,
  PageNotFound,
  RequestError,
  ParseError,
  Success,  
};

public enum Request
{
    GET,
    POST,
    PUT,
    DELETE,
};

public partial class StepSymphonyBackend: MonoBehaviour {

    
    
    //Properties
    public string BackendUrl
    {
        get
        {
            return useProduction ? productionUrl : developmentUrl;
        }
    }

    //URLS
    public bool useProduction = false;
    public bool isSecure = false;
    public string productionUrl = "http://stepsymphony:8000/";
    public string developmentUrl = "http://localhost:8000/";

    //Methods for talking to the database
    private void Send(Request rtype, string command, WWWForm wwwForm, RequestResponseDelegate onResponse = null, string authToken = "")
    {
        //webpage for the request
        WWW request;
        //django headers
#if UNITY_5_PLUS
        Dictionary<string, string> headers;
#else
        Hashtable headers;
#endif
        //POST Data
        byte[] postData;
        string url = BackendUrl + command;

        if (isSecure)
        {
            url = url.Replace("http", "https");
        }

        if(wwwForm == null) //if there is no webform, create one
        {
            wwwForm = new WWWForm();
            postData = new byte[] { 1 };
        }
        else
        {
            postData = wwwForm.data;
        }

        headers = wwwForm.headers;

        //make sure to get a json response
        headers.Add("Accept", "application/json");

        //add the correct request
        headers.Add("X-UNITY-METHOD", rtype.ToString());

        //also add authentication token if provided
        if(authToken != "")
        {
            headers.Add("Authorization", "Token " + authToken);
        }
        request = new WWW(url, postData, headers);

        System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
        string callee = stackTrace.GetFrame(1).GetMethod().Name;
        StartCoroutine(HandleRequest(request, onResponse, callee));
        
    }

    private IEnumerator HandleRequest(WWW request, RequestResponseDelegate onResponse, string callee)
    {
        //wait until the request is processed
        while (true)
        {
            if (request.isDone)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }

        //catch the errors from the client
        if (!string.IsNullOrEmpty(request.error))
        {
            if(onResponse != null)
            {
                onResponse(Response.ClientError, null, callee);
            }
            yield break;
        }
        int statusCode = 200;

        if (request.responseHeaders.ContainsKey("REAL_STATUS"))
        {
            string status = request.responseHeaders["REAL_STATUS"];
            statusCode = int.Parse(status.Split(' ')[0]);
        }
        bool responseSuccessful = (statusCode >= 200 && statusCode <= 206);
        JToken responseObj = null;

        try
        {
            if (request.text.StartsWith("["))
                responseObj = JArray.Parse(request.text);
            else
                responseObj = JObject.Parse(request.text);
        }
        catch(Exception ex)
        {
            if(onResponse != null)
            {
                if (!responseSuccessful)
                {
                    if(statusCode == 404)
                    {
                        //cannot parse a non existing page
                        Debug.LogWarning("Page Not Found: " + request.url);
                        onResponse(Response.PageNotFound, null, callee);
                    }
                    else
                    {
                        Debug.Log("Couldn't parse the response: " + request.text);
                        Debug.Log("Exception = " + ex.ToString());
                        onResponse(Response.ParseError, null, callee);
                    }
                }
                else
                {
                    if(request.text == "")
                    {
                        onResponse(Response.Success, null, callee);
                    }
                    else
                    {
                        Debug.Log("Couldn't parse the response: " + request.text);
                        Debug.Log("Exception = " + ex.ToString());
                        onResponse(Response.ParseError, null, callee);
                    }
                }
            }
            yield break;
        }

        if (!responseSuccessful)
        {
            if (onResponse != null)
                onResponse(Response.RequestError, responseObj, callee);
            yield break;
        }

        //successful response
        if(onResponse != null)
        {
            onResponse(Response.Success, responseObj, callee);
        }
    }
}
