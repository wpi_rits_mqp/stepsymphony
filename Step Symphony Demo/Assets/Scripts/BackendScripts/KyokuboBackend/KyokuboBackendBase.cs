﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Newtonsoft.Json.Linq;

public partial class KyokuboBackend {

    public delegate void RequestResponseDelegate(Response responseType, JToken jsonResponse, string callee);

    //Retrieve Info Delegate Methods
    public delegate void RetrieveInfoFail(string errorMsg);
    public delegate void RetrieveInfoSuccess(List<Kyokubou> kyokuboData);
    public RetrieveInfoFail OnRetrieveInfoFail;
    public RetrieveInfoSuccess OnRetrieveInfoSuccess;
    
    /// <summary>
    /// Sends a GET Request to the kyokubo database,
    /// from there it will call the appropriate delegate if successful or failed
    /// </summary>
    public void RetrieveInfo()
    {
        //creates a new WWWForm
        WWWForm webform = new WWWForm();
        Send(Request.GET, "api/kyoinfo/", webform, OnRetrieveInfoResponse);
    }
    
    /// <summary>
    /// Gets the kyokubo information from the kyokubo server and puts it into a string to be parsed by the game
    /// If it cannot be called from the server, an error message will get passed
    /// </summary>
    /// <param name="responseType"></param>
    /// <param name="responseData"></param>
    /// <param name="callee"></param>
    private void OnRetrieveInfoResponse(Response responseType, JToken responseData, string callee)
    {
        if(responseType == Response.Success)
        {
            if(OnRetrieveInfoSuccess != null)
            {
                OnRetrieveInfoSuccess(JsonUtility.FromJson<List<Kyokubou>>(responseData.ToString()));
                Debug.Log("Reached the success method");
                
            }
        }
        else
        {
            if(OnRetrieveInfoFail != null)
            {
                OnRetrieveInfoFail("Could not acquire kyokubo information from the server!");
                Debug.LogWarning("Could not acquire kyokubo information from the server!");
            }
        }
    } 
	
}
