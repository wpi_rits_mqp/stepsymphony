﻿// Copyright (c) 2015 Eamon Woortman
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
// Modified by: Marco Duran to incorperate the use of storing locations and updating user progress

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

public partial class StepSymphonyBackend {
    //Public Delegates
    public delegate void RequestResponseDelegate(Response responseType, JToken jsonResponse, string callee);
    //Login Delagtes and Methods
    public delegate void LoginFail(string errorMsg);
    public delegate void LoggedIn();
    public LoggedIn OnLoggedIn;
    public LoginFail OnLoginFail;
    //Signup Delegates and Methods
    public delegate void SignupFail(string errorMsg);
    public delegate void SignupSuccess();
    public SignupSuccess OnSignupSuccess;
    public SignupFail OnSignupFail;
    //Save Progress Delegates and Methods
    public delegate void SaveGameSuccess();
    public delegate void SaveGameFail(string errorMsg);
    public SaveGameSuccess OnSaveGameSuccess;
    public SaveGameFail OnSaveGameFail;
    //Load Game Progress Delegates and Methods
    public delegate void GameLoaded(string saveData);
    public delegate void GameLoadedFail(string errorMsg);
    public GameLoaded OnGameLoaded;
    public GameLoadedFail OnGameLoadedFail;
    /// <summary>
    /// Kyokubou DB Stuff
    /// </summary>
    //Retrieve Info Delegate Methods
    public delegate void RetrieveInfoFail(string errorMsg);
    public delegate void RetrieveInfoSuccess(List<Kyokubou> kyokuboData);
    public RetrieveInfoFail OnRetrieveInfoFail;
    public RetrieveInfoSuccess OnRetrieveInfoSuccess;

    //An authentication token that will be used while a specific player is logged in
    string authToken = "";

    //In order to sign up, must first do a POST request to the db,
    //and try to get authentication. When successful, the auth token will be saved
    //When this is successful, OnSignupSuccess delegate will be called.
    //When this fails, OnSignupFailed delegate will be called
    public void Signup(string username, string email, string password)
    {
        WWWForm webform = new WWWForm();
        webform.AddField("username", username);
        webform.AddField("email", email);
        webform.AddField("password", password);
        Send(Request.POST, "player_db/api/user", webform, OnSignupResponse);
    }
    //Signup Response, checks to see if authentication of signup is successful
    private void OnSignupResponse(Response responseType, JToken responseData, string callee)
    {
        if (responseType == Response.Success)//Server is up and running and has correct credentials
        {
            if (OnSignupSuccess != null)
            {
                OnSignupSuccess();
            }
        }
        else if (responseType == Response.ClientError)//Could not reach the server
        {
            if (OnSignupFail != null)
            {
                OnSignupFail("Could not reach the server. Please try again at a later time.");
            }
        }
        else if (responseType == Response.RequestError)//Validates all information in the fields
        {
            string errors = "";
            JObject obj = (JObject)responseData;
            foreach (KeyValuePair<string, JToken> pair in obj)
            {
                errors += "[" + pair.Key + "] ";
                foreach (string errorStr in pair.Value)
                {
                    errors += errorStr;
                }
                errors += '\n';
            }
            if (OnSignupFail != null)
            {
                OnSignupFail(errors);
            }
        }
    }

    //In order to log in, must first do a POST request to the db, will try to get another authentication token. On success, it will save for further use.
    //If successful, Call the OnLoginSuccess delegate.
    //If failed, call the OnLoginFail delegate.
    public void Login(string username, string password)
    {
        WWWForm webform = new WWWForm();
        //validate that the string for email is actually an email
        webform.AddField("username", username);
        webform.AddField("password", password);
        Send(Request.POST, "player_db/api/getauthtoken", webform, OnLoginResponse);
    }
    //Login Response, checks to see if authentication of login is successful
    private void OnLoginResponse(Response responseType, JToken responseData, string callee)
    {
        if (responseType == Response.Success)
        {
            authToken = responseData.Value<string>("token");
            if (OnLoggedIn != null)
            {
                OnLoggedIn();
            }
        }
        else if (responseType == Response.ClientError)
        {
            if (OnLoginFail != null)
            {
                OnLoginFail("Could not reach the server, please try again at a later time.");
            }
        }
        else //Checks to see if all of the fields have the appropriate content (checks valid email and password)
        {
            JToken fieldToken = responseData["non_field_errors"];
            if (fieldToken == null || !fieldToken.HasValues)
            {
                if (OnLoginFail != null)
                    OnLoginFail("Login failed, unknown error.");
            }
            else
            {
                string errors = "";
                JToken[] fieldValidationErrors = fieldToken.Values().ToArray();
                foreach (JToken validationError in fieldValidationErrors)
                {
                    errors += validationError.Value<string>();
                }
                if (OnLoginFail != null)
                    OnLoginFail("Login failed: " + errors);
            }
        }
    }
    /// <summary>
    /// Posts the Savedata to the database. Call appropriate delegate methods indicating success and failure
    /// </summary>
    /// <param name="save"></param>
    public void SaveGame(string username, string saveDataPath)
    {
        //makes sure to save the game on unity's end first
        //call local save first then database save
        WWWForm form = new WWWForm();
        form.AddField("owner", username);
        form.AddBinaryData("saveData", System.Text.Encoding.UTF8.GetBytes(saveDataPath));
        Send(Request.POST, "player_db/api/save", form, OnSaveGameResponse, authToken);
    }
    /// <summary>
    /// What to do when you recieve a response from the server
    /// </summary>
    /// <param name="responseType"></param>
    /// <param name="responseData"></param>
    /// <param name="callee"></param>
    private void OnSaveGameResponse(Response responseType, JToken responseData, string callee)
    {
        if (responseType == Response.Success)
        {
            if(OnSaveGameSuccess != null)
            {
                OnSaveGameSuccess(); //the file where this is called is located in...Capture success
            }
        }
        else if(responseType == Response.ClientError)
        {
            if(OnSaveGameFail != null)
            {
                OnSaveGameFail("Cannot reach the server.");
            }
        }
        else
        {
            
            if(OnSaveGameFail != null)
            {
                OnSaveGameFail("Request Failed: " + responseType + "-" + responseData["detail"]);
            }
        }
    }

    /// <summary>
    /// Gets the save data from the server and calls the response function
    /// </summary>
    public void LoadGame()
    {
        WWWForm form = new WWWForm();
        Send(Request.GET, "player_db/api/save/", form, OnLoadGameResponse, authToken);
    }

    /// <summary>
    /// If the response returns success, call the success delegate, if not call the failure delegate
    /// </summary>
    /// <param name="responseType"></param>
    /// <param name="responseData"></param>
    /// <param name="callee"></param>
    private void OnLoadGameResponse(Response responseType, JToken responseData, string callee)
    {
        if(responseType == Response.Success)
        {
            if(OnGameLoaded != null)
            {
                OnGameLoaded(JsonUtility.FromJson<string>(responseData.ToString()));
            }
        }
        else
        {
            if(OnGameLoadedFail != null)
            {
                OnGameLoadedFail("Error, could not acquire your save data, please try again later.");
            }
        }
    }

    /**************************************KyokuboDB**************************************/
    /*************************************************************************************/
    /// <summary>
    /// Sends a GET Request to the kyokubo database,
    /// from there it will call the appropriate delegate if successful or failed
    /// </summary>
    public void RetrieveInfo()
    {
        
        Send(Request.GET, "kyokubo_db/api/kyoinfo/", null, OnRetrieveInfoResponse, authToken);
    }

    /// <summary>
    /// Gets the kyokubo information from the kyokubo server and puts it into a string to be parsed by the game
    /// If it cannot be called from the server, an error message will get passed
    /// </summary>
    /// <param name="responseType"></param>
    /// <param name="responseData"></param>
    /// <param name="callee"></param>
    private void OnRetrieveInfoResponse(Response responseType, JToken responseData, string callee)
    {
        if (responseType == Response.Success)
        {
            if (OnRetrieveInfoSuccess != null)
            {
                OnRetrieveInfoSuccess(JsonConvert.DeserializeObject<List<Kyokubou>>(responseData.ToString()));
                Debug.Log("Reached the success method");

            }
        }
        else
        {
            if (OnRetrieveInfoFail != null)
            {
                OnRetrieveInfoFail("Could not acquire kyokubo information from the server!");
                Debug.LogWarning("Could not acquire kyokubo information from the server!");
            }
        }
    }
}
