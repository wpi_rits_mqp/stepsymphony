﻿using UnityEngine;
using System.Collections;
using System;

public class KyokuboLoc : MonoBehaviour
{

    //Holds latitude and longitude of Kyokubou
    public double lat, lon;

    public int currentKyokubou;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start ()
    {

    }

    public void increment()
    {
        currentKyokubou = currentKyokubou + 1;
    }

    // Update is called once per frame
    void Update ()
    {
	
	}
}

[Serializable]
public class Location {
    public string locname;
    public double latitude, longitude;

    public Location(string locname = "", double latitude = 0.0, double longitude = 0.0)
    {
        if(locname != "")
            this.locname = locname;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}