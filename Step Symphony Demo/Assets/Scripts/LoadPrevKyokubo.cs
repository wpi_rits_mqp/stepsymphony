﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoadPrevKyokubo : MonoBehaviour {
    //sprite renderer of the kyokubo
    SpriteRenderer kyoSpriteRender;
    Dictionary<string, string> kyoTable;
    public string kyokuboName;
    public Sprite spriteToLoad;

    // Use this for initialization
    void Start () {
        //fill the dictionary
        kyoTable = new Dictionary<string, string>();
        kyoTable.Add("Bunny", "Bunny_Art");
        kyoTable.Add("Fox", "Fox_Art_BambooFlute");
        kyoSpriteRender = GetComponent<SpriteRenderer>();
        kyokuboName = PlayerPrefs.GetString("Kyokubo Last Touched");
    }

    void Awake()
    {
        
    }
	
	// Update is called once per frame
	void Update () {
        if (kyoSpriteRender.sprite == null)
        {
            //check the dictionary to see if the value is there
            if (kyoTable.ContainsKey(kyokuboName))
            {
                //load sprite with the name of the kyokubo
                LoadSprite(kyokuboName);
            }
        }
    }

    //finds the key provided by the name in the dictionary 
    void LoadSprite(string kyoName)
    {
        string spriteName = kyoTable[kyoName];
        spriteToLoad = Resources.Load(spriteName, typeof(Sprite)) as Sprite;
        kyoSpriteRender.sprite = spriteToLoad;
        

    }
}
