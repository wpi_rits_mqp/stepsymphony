﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public class KyokuboBehavior : MonoBehaviour {
    //name of the kyokubo
    public string kyokuboName;
    //touch box of the kyokubo
    //public BoxCollider touchBox;
    //touch pos for the finger
    public Vector3 touchPos;
    //Ray for raycasting
    public Ray touchRay;
    //raycast hit for the touch of the finger
    public RaycastHit touchInfo;
    //bool to check if the kyokubo has been touched
    public bool isTouched;
    //Absolute distance script
    //public FindAbsoluteDistance distScript;
    //double of the distance from the center of the camera to the kyokubo
    //public double kyokuboDist;

    //sprite renderer of the kyokubo
    SpriteRenderer kyoSpriteRender;
    Dictionary<string, string> kyoTable;
    int tableCount;
    GameObject kyokubo;

    Scene arScene;
    Scene captureScene;

	// Use this for initialization
	void Start () {
        kyokubo = gameObject;
        arScene = SceneManager.GetSceneByName("ARScreen");
        captureScene = SceneManager.GetSceneByName("CaptureScreen");
        isTouched = false;
	}
	
	// Update is called once per frame
	void Update () {
        //check to see if the player has touched the screen
        if(Input.touchCount > 0 && Input.touchCount < 2)
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            touchRay = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            if(Physics.Raycast(touchRay, out touchInfo) && touchInfo.transform.tag == "Kyokubo")
            {
                isTouched = true;
                EnterCapture(kyokuboName);
            }
        }
    }

    public void EnterCapture(string kyoName)
    {
        isTouched = false;
        // Does not work for android in unity 5.3
        //pass name last pressed to player prefs
        PlayerPrefs.SetString("Kyokubo Last Touched", kyoName);
        PlayerPrefs.Save();
        
        //load the scene
        SceneManager.LoadScene("RhythmCaptureScreen");
    }
}


