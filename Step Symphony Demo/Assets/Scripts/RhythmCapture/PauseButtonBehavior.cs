﻿using UnityEngine;
using System.Collections;

public class PauseButtonBehavior : MonoBehaviour {
    
    public GameObject pauseMenu;//game object for the pause panel
    
    public double lastSongPos;//last known song position
    
    public Vector2 lastKyoLoc;//keep track of kyokubo's position before pause
    
    public AudioSource song;//the song to pause
    
    RhythmConductor conductorScript;//conductor script

    public bool isPaused = false;
    
    // Use this for initialization
    void Start () {
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        song = GameObject.Find("Conductor").GetComponent<AudioSource>();
        conductorScript = GameObject.Find("Conductor").GetComponent<RhythmConductor>();

        if (!isPaused)
            pauseMenu.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    /*Pause the game by setting the timescale to zero
     *so the notes stop falling and pausing the audio source.
     *After this is done, make the pause menu appear, 
     *this will allow the player to resume the song or go back to the map.
     */
    public void PauseGame()
    {
        if (!isPaused)
        {
            isPaused = true;
            //check to see if the pause panel exists in hierarchy
            if (pauseMenu != null)
            {
                //pause the music
                if(song != null)
                {
                    song.Pause();
                }
                //set timescale to 0
                Time.timeScale = 0;
                //set last song position 
                lastSongPos = conductorScript.songPosition;

                //open up the pause menu
                pauseMenu.gameObject.SetActive(true);
            }
        }
    }

    /*Resume the game by setting the songposition to the last recorded
     * Setting the song position to the variable 
     * set the timescale to 1 and play the music
     * 
     */
    public void ResumeGame()
    {
        if (isPaused)
        {
            //set the song position
            conductorScript.songPosition = lastSongPos;
            isPaused = false;
            //close the pause menu
            pauseMenu.gameObject.SetActive(false);
            //set the timescale to 1
            Time.timeScale = 1;
            //resume the song
            if(song != null)
            {
                song.UnPause();
            }
        }
    }

    /*Go back to the map screen
     * 
     */
    public void GoToMap()
    {
        //indicated capture failure 
    }
}
