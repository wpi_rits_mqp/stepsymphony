﻿using UnityEngine;
using System.Collections;

public class NoteData : MonoBehaviour {
    //type of note, can be either short or long
    string noteType;

    public double duration; //if the note duration is below a certain threshold, it will be a single note, else, it is a long note

    public bool isHeld; //checks to see if the note is held down

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
