﻿using UnityEngine;
using System.Collections;

public class NoteCatcher1Behavior : MonoBehaviour {

    public bool isColliding;

    public GameObject temp_note;

    public StrumTrack1 strumScript;

    public UpdateScore scoreScript;

    //public GameObject Kyokubou;

    ParticleSystem noteCatcherParticleSystem;

    //public int trackNum = 1;

    public bool isPressed;

    public bool hitSuccess;
	// Use this for initialization
	void Start () {
        scoreScript = GameObject.Find("ScorePanel").GetComponentInChildren<UpdateScore>();
        strumScript = GameObject.Find("TapButton1").GetComponent<StrumTrack1>();
        //Kyokubou = GameObject.FindGameObjectWithTag("Kyokubou");
        noteCatcherParticleSystem = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        isPressed = strumScript.isPressed;
        if (isPressed && !isColliding)
        {
            hitSuccess = false;
        }
        
	}
    //if there is more than one object colliding with the trigger,
    //only delete the first one
    //put this in the note catcher behavior
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Note1"))
        {
            isColliding = true;
            temp_note = col.gameObject;
            if (isPressed)
            {
                hitSuccess = true;
                noteCatcherParticleSystem.Play();
                Destroy(temp_note);
                isColliding = false;
                scoreScript.score = scoreScript.score + 1;
                strumScript.isPressed = false;

            }
            else
            {
                hitSuccess = false;
            }
         }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        isColliding = false;
        if (col.gameObject.CompareTag("Note1"))
        {
            Destroy(col.gameObject);
        }
        if (!hitSuccess)
        {
            scoreScript.score = scoreScript.score - 1;
            //if(Kyokubou != null)
            //{
                //Kyokubou.GetComponent<RectTransform>().

            //}
        }
            
    }
}
