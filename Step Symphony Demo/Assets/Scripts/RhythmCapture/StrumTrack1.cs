﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class StrumTrack1 : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler {
    //this should be attached to the button of the corresponding note catcher

    public GameObject noteCatcher;
    Renderer obj_renderer;
    public Material press_material;
    public Material depress_material;

    public UpdateScore scoreScript;
    public NoteCatcher1Behavior noteCatcherBehaviorScript; 

    //public int trackNum = 1;

    //note object to destroy when strumming
    public GameObject temp_note;

    //checks to see if the note is colliding with the note catcher
    public bool isColliding = false;

    //is the button pressed?
    public bool isPressed = false;

    //have you hit the note in time?
    public bool hitSuccess = false;

	// Use this for initialization
	void Start () {
        //initialize scoreScript from the canvas
        scoreScript = GameObject.Find("ScorePanel").GetComponentInChildren<UpdateScore>();
     
        noteCatcher = GameObject.Find("NoteCatcher1");
        //initialize note catcher behavior script
        noteCatcherBehaviorScript = noteCatcher.GetComponent<NoteCatcher1Behavior>();
        obj_renderer = noteCatcher.GetComponent<Renderer>();
            
        
        
        press_material = Resources.Load("ButtonPressedMaterial", typeof(Material)) as Material;
        depress_material = Resources.Load("ButtonDepressedMaterial", typeof(Material)) as Material;
    }
	
	// Use fixed update for reduced latency between objects
	void FixedUpdate () {
        isColliding = noteCatcherBehaviorScript.isColliding;
        hitSuccess = noteCatcherBehaviorScript.hitSuccess;
        if (hitSuccess)
        {
            isPressed = false;
        }
	}

    public void OnPointerClick(PointerEventData pointer)
    {
        //isPressed = true;
       StartCoroutine(StrumNote());
        //isPressed = false;
    }

    public void OnPointerDown(PointerEventData pointer)
    { 
        if (obj_renderer != null)
        {
            obj_renderer.material = press_material;
        }
        isPressed = true;
        //StartCoroutine(StrumNote());
    }

    public void OnPointerUp(PointerEventData pointer)
    {
        if (obj_renderer != null)
        {
            obj_renderer.material = depress_material;
        }
        isPressed = false;
    }
    

    //checks to see if the note is over the note catcher, 
    //if the note is over the note catcher, delete the note and increment the score
    //else, decrement the score
    public IEnumerator StrumNote()
    {
        //isPressed = false;
        if (!hitSuccess && !isColliding)
        {
            //here would be the place to check if the player is early, perfect, or late
            scoreScript.score = scoreScript.score - 1;
            yield return false;
        }
        yield return true;
    }
}
