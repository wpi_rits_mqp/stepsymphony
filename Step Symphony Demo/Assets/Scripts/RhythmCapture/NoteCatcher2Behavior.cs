﻿using UnityEngine;
using System.Collections;

public class NoteCatcher2Behavior : MonoBehaviour {
    public bool isColliding;

    public GameObject temp_note;

    public StrumTrack2 strumScript;

    public UpdateScore scoreScript;

    private ParticleSystem noteCatcherParticleSystem;

    public bool isPressed;

    public bool hitSuccess;
    // Use this for initialization
    void Start()
    {
        scoreScript = GameObject.Find("ScorePanel").GetComponentInChildren<UpdateScore>();
        strumScript = GameObject.Find("TapButton2").GetComponent<StrumTrack2>();
        noteCatcherParticleSystem = GetComponent<ParticleSystem>();

    }

    // Update is called once per frame
    void Update()
    {
        isPressed = strumScript.isPressed;
        if (isPressed && !isColliding)
        {
            hitSuccess = false;
        }

    }

    //put this in the note catcher behavior
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Note2"))
        {
            isColliding = true;
            temp_note = col.gameObject;
            if (isPressed)
            {
                hitSuccess = true;
                noteCatcherParticleSystem.Play();
                Destroy(temp_note);
                isColliding = false;
                scoreScript.score = scoreScript.score + 1;
                strumScript.isPressed = false;
            }
            else
            {
                hitSuccess = false;
            }
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        isColliding = false;
        if (col.gameObject.CompareTag("Note2"))
        {
            Destroy(col.gameObject);
        }
        if(!hitSuccess)
            scoreScript.score = scoreScript.score - 1;
    }
}
