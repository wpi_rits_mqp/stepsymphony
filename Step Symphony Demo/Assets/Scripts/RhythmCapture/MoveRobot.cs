﻿using UnityEngine;
using System.Collections;

public class MoveRobot : MonoBehaviour {
    //get the score ofthe game
    public UpdateScore scoreScript;
    public int lastScore;
    Vector2 nextSuccessPos;
    Vector2 nextFailPos;
    Vector2 currentPos;
    RhythmConductor conductorScript;
    PauseButtonBehavior pauseScript;

    public bool upCR = false;
    public bool downCR = false;
    public float lerpValue = 0.05f;
	// Use this for initialization
	void Start () {
        conductorScript = GameObject.Find("Conductor").GetComponent<RhythmConductor>();
        scoreScript = GameObject.Find("ScorePanel").GetComponentInChildren<UpdateScore>();
        pauseScript = GameObject.FindGameObjectWithTag("PauseButton").GetComponent<PauseButtonBehavior>();
        if(scoreScript != null)
        {
            lastScore = scoreScript.score;
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (!pauseScript.isPaused)
        {
            currentPos = transform.position;
            //lastScore = scoreScript.score;
            //check to see if the score of the player has increased
            if (scoreScript.score > lastScore)
            {
                StartCoroutine(MoveDown());
                if (upCR)
                {
                    StopCoroutine(MoveUp());
                    upCR = false;
                }
                
            }
            else if (scoreScript.score < lastScore)
            {
                StartCoroutine(MoveUp());
                if (downCR)
                {
                    StopCoroutine(MoveDown());
                    downCR = false;
                }
                
            }
        }
	}

    IEnumerator MoveDown()
    {
        downCR = true;
        if (scoreScript.score > lastScore)
        {
            lastScore = scoreScript.score;
            //set where the kyokubo will go next
            nextSuccessPos = new Vector2(currentPos.x, currentPos.y - lerpValue);
            //move the kyokubo to its current position 
            transform.position = Vector2.Lerp(transform.position, nextSuccessPos, (float)conductorScript.trackSpeed * 5);
            yield return null;
        }
    }

    IEnumerator MoveUp()
    {
        upCR = true;
        if(scoreScript.score < lastScore)
        {
            lastScore = scoreScript.score;
            //set the next position of the kyokubo
            nextFailPos = new Vector2(currentPos.x, currentPos.y + lerpValue);
            //move the kyokubo to the next position 
            transform.position = Vector2.Lerp(transform.position, nextFailPos, (float)conductorScript.trackSpeed * 5);
            yield return null;
        }
        
        
    }
}
