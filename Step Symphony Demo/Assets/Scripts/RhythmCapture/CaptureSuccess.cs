﻿using UnityEngine;
using System.Collections;

public class CaptureSuccess : MonoBehaviour {

    //canvas behavior script
    CanvasBehavior canvasScript;
    //PlayerData playerData;
    public Kyokubou kyoToCollect;
	// Use this for initialization
	void Awake () {
        canvasScript = GameObject.Find("Canvas").GetComponentInChildren<CanvasBehavior>();
        kyoToCollect = GameObject.FindGameObjectWithTag("Kyokubo").GetComponent<Kyokubou>();
    }

    void Update()
    {
        //Add the 
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //you win the game
        if (col.gameObject.CompareTag("Kyokubo"))
        {
            //Destroy(col.gameObject);
            //col.gameObject.SetActive(false);
            //canvasBehavior method, capturesuccess
            canvasScript.CaptureSuccess();

            //add the kyokubou to the dictionary
            //if(!PlayerData.playerData.kyokuboCollection.ContainsKey(kyoToCollect.kyoname))
            if(kyoToCollect != null)
                PlayerData.playerData.AddKyokuboToCollection(kyoToCollect.kyoname);
            PlayerData.playerData.kyokuboCollection.Add(kyoToCollect);
            PlayerData.playerData.SaveProgress();
        }
    }
}
