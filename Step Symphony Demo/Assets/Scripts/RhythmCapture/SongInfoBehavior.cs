﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.IO;



public class SongInfoBehavior : MonoBehaviour {
    public Dictionary<double, double> songInfo; //dictionary of start time, with a value of duration
    int trackNum; //this will be the track number that the note value will be assigned to

    public TextAsset songFile; //xml file that holds all of the song data, key is song position value is duration
    public string fileName;

    public List<double> songPositions;
    public List<double> noteDurations;

    //get these values from the xml file
    public double songDuration = 0.0;
    public double songTempo = 0.0f;
    public string songTitle = "";

    public double trackSpeed;

    //mult value is max time pos/100
    //so find max time position
    double multValue;
    GameObject conductor;
    RhythmConductor conductorScript;

    public bool isLoaded = false;
	// Use this for initialization
	void Start () {
        conductor = GameObject.Find("Conductor");
        conductorScript = conductor.GetComponent<RhythmConductor>();

        if(fileName == null)
        {
            fileName = "";
        }

        //songFile = Application.dataPath + "/SongXmls/" + fileName;
        songFile = (TextAsset)Resources.Load("SongXmls/" + fileName, typeof(TextAsset));
        if (songFile != null)
        {
            //string xmlContent = System.IO.File.ReadAllText(songFile.text);
            XDocument xmlFile = XDocument.Parse(songFile.text);
            
            songInfo = new Dictionary<double, double>();
                     
            //go through each XML node of song info to collect the note data for the file
            foreach(var note in xmlFile.Root.Elements("SongInfo"))
            {
                if(note.Attribute("SongDur") != null)
                {
                    if (double.TryParse(note.Attribute("SongDur").Value, out songDuration))
                    {
                        if (conductorScript.durationInSamples != 0.0f)
                            multValue = (conductorScript.durationInSamples/44100 - conductorScript.offset) / songDuration;
                    }
                }
                if(note.Attribute("Tempo") != null)
                {
                    if(double.TryParse(note.Attribute("Tempo").Value, out songTempo))
                        trackSpeed = (60.0 / songTempo) / conductorScript.crotchetsPerBar;//set the track speed once it has been found
                }
                if(note.Attribute("Title") != null && note.Attribute("Title").Value.Length > 0)
                    songTitle = note.Attribute("Title").Value;
                if(note.Attribute("SongPos") != null && note.Attribute("NoteDur") != null)
                {
                    songInfo.Add(double.Parse(note.Attribute("SongPos").Value), double.Parse(note.Attribute("NoteDur").Value));
                    songPositions.Add((float.Parse(note.Attribute("SongPos").Value) * multValue)-trackSpeed);
                    noteDurations.Add(double.Parse(note.Attribute("NoteDur").Value));
                }
            }
            if(songPositions.Count == songInfo.Count && songDuration != 0.0f && songTempo != 0.0f)
            {
                
                //multValue = 100 / songDuration;
                isLoaded = true;
            }
                
        }
        


    }
	
	// Update is called once per frame
	void Update () {
	
	}

    
}
