﻿using UnityEngine;
using System.Collections;

public class LerpNote : MonoBehaviour {
    //positions which the note will start from
    public Vector3 track1StartPos;
    public Vector3 track2StartPos;
    //positions which the note will disappear from
    public Vector3 track1EndPos;
    public Vector3 track2EndPos;

    //track to spawn note at
    public int trackNum = 1;

    //Note prefab to spawn
    public GameObject NotePrefab;
    GameObject note;

    //script for the conductor
    RhythmConductor conductorScript;
    public double trackSpeed;




    // Use this for initialization
    void Start () {
        conductorScript = GameObject.Find("Conductor").GetComponent<RhythmConductor>();
        trackSpeed = conductorScript.bpm;
        /**If any of the track positions are not set, set them based on the positions in the heiarchy if they exist **/
        if (track1StartPos == Vector3.zero)
        {
            if(GameObject.Find("NoteSpawner1") != null)
                track1StartPos = GameObject.Find("NoteSpawner1").transform.position;
        }
        if(track2StartPos == Vector3.zero)
        {
            if (GameObject.Find("NoteSpawner2") != null)
                track2StartPos = GameObject.Find("NoteSpawner2").transform.position;
        }
        if(track1EndPos == Vector3.zero)
        {
            if (GameObject.Find("NoteCatcher1") != null)
                track1EndPos = GameObject.Find("NoteCatcher1").transform.position;
        }
        if(track2EndPos == Vector3.zero)
        {
            if (GameObject.Find("NoteCatcher2") != null)
                track2EndPos = GameObject.Find("NoteCatcher2").transform.position;
        }
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //spawns the note at the start point
    public void CreateNote()
    {

        /*if(trackNum == 1) //spawn note at start point at track number 1
        {
            note = Instantiate(NotePrefab, new Vector3(track1StartPos.x, track1StartPos.y, -2), new Quaternion(0, 0, 0, 0)) as GameObject;
            MoveNote();
        }
        else if(trackNum == 2) //spawn note at start point at track number 2
        {
            note = Instantiate(NotePrefab, new Vector3(track2StartPos.x, track2StartPos.y, -2), new Quaternion(0, 0, 0, 0)) as GameObject;
            MoveNote();
        }
        else //ya got a problem there, there is no third track
        {
            print("How did you even get here?");
        } */

        switch (trackNum)
        {
            case 1:
                note = Instantiate(NotePrefab, new Vector3(track1StartPos.x, track1StartPos.y, track1StartPos.z), new Quaternion(0, 0, 0, 0)) as GameObject;
                StartCoroutine(MoveNote1(note));
                break;

            case 2:
                note = Instantiate(NotePrefab, new Vector3(track2StartPos.x, track2StartPos.y, track2EndPos.z), new Quaternion(0, 0, 0, 0)) as GameObject;
                StartCoroutine(MoveNote2(note));
                break;

            default:
                print("Error, no track number specified.");
                break;

        }
    }

    //Moves the note on track 1 from the start of the track to the end of the track based on the tempo of the song
    IEnumerator MoveNote1(GameObject note_obj)
    {
        print("Coroutine 1 started");
        if(note_obj != null)
        {
            while (note_obj != null && Vector2.Distance(note_obj.transform.position, track1EndPos) > 0.05f)
            {
                //print("Hello?");
                note_obj.transform.position = Vector3.MoveTowards(note_obj.transform.position, track1EndPos, (float)trackSpeed);//must change to time of song eventually

                yield return null;
            }
            print("Reached the end of track 1");
        }
    }

    //Moves the note on track 2 from the start of the track to the end of the track based on the tempo of the song
    IEnumerator MoveNote2(GameObject note_obj)
    {
        print("Coroutine 2 started");
        if(note_obj != null)
        {
            while (note_obj != null && Vector2.Distance(note_obj.transform.position, track2EndPos) > 0.05f)
            {
                note_obj.transform.position = Vector3.MoveTowards(note_obj.transform.position, track2EndPos, (float)trackSpeed);//must change to time of song eventually
                yield return null;
            }
            print("Reached the end of track 2");
        }
        

    }
}
