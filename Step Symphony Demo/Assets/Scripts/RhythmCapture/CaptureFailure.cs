﻿using UnityEngine;
using System.Collections;

public class CaptureFailure : MonoBehaviour {
    //completion panel
    //public GameObject completionPanel;
    //failure panel
    //public GameObject failureImage;
    //public GameObject collectionsButton;
    //public GameObject mapButton;
    CanvasBehavior canvasScript;

    //get script of the conductor to get the capture music 
    //RhythmConductor conductorScript;
    // Use this for initialization
	void Start () {
        //completionPanel = GameObject.Find("CompletionPanel");
        //failureImage = GameObject.Find("FailureImage");
        //collectionsButton = GameObject.Find("CollectionsButton");
        //mapButton = GameObject.Find("MapButton");
        //conductorScript = GameObject.Find("Conductor").GetComponent<RhythmConductor>();

        //set all of these to inactive
        //if(completionPanel != null && failureImage != null && collectionsButton != null && mapButton != null)
        //{
        //    completionPanel.gameObject.SetActive(false);
        //}
        canvasScript = GameObject.Find("Canvas").GetComponentInChildren<CanvasBehavior>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Kyokubo"))
        {
            Destroy(col.gameObject);
            canvasScript.CaptureFailure();
            

        }
        
    }
}
