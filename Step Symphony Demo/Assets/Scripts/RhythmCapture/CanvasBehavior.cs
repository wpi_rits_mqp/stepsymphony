﻿using UnityEngine;
using System.Collections;

public class CanvasBehavior : MonoBehaviour {
    public GameObject completionPanel;
    public GameObject successImage;
    public GameObject failureImage;
    public GameObject collectionsButton;
    public GameObject mapButton;

    RhythmConductor conductorScript;



	// Use this for initialization
	void Start () {
        completionPanel = GameObject.Find("CompletionPanel");
        failureImage = GameObject.Find("FailureImage");
        collectionsButton = GameObject.Find("CollectionsButton");
        mapButton = GameObject.Find("MapButton");
        conductorScript = GameObject.Find("Conductor").GetComponent<RhythmConductor>();

        //set these all inactive on load
        if (completionPanel.gameObject.activeInHierarchy)
        {
            completionPanel.gameObject.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CaptureSuccess()
    {
        if (completionPanel != null)
            completionPanel.gameObject.SetActive(true);
        if (successImage != null)
            successImage.gameObject.SetActive(true);
        if (collectionsButton != null)
            collectionsButton.gameObject.SetActive(true);
        if (mapButton != null)
            mapButton.gameObject.SetActive(true);
        if (failureImage != null)
            failureImage.gameObject.SetActive(false);

        KyokuboLoc yes = GameObject.FindGameObjectWithTag("Respawn").GetComponentInChildren<KyokuboLoc>();
        yes.increment();

        //check to see if capture music is playing
        if (conductorScript.song.isPlaying)
        {
            conductorScript.song.Stop();
            //then play success music here
            //and also maybe a particle effect here?
        }
    }

    public void CaptureFailure()
    {
        if (completionPanel != null)
            completionPanel.gameObject.SetActive(true);
        if (failureImage != null)
            failureImage.gameObject.SetActive(true);
        if (successImage != null)
            successImage.gameObject.SetActive(false);
        //also make the button to go back to the map screen
        if (mapButton != null)
            mapButton.SetActive(true);

        collectionsButton.gameObject.SetActive(false);
        //check to see if capture music is playing
        if (conductorScript.song.isPlaying)
        {
            //if so, stop it
            conductorScript.song.Stop();
            //then play failure music
            //and maybe put a particle effect here?
        }
    }
}

