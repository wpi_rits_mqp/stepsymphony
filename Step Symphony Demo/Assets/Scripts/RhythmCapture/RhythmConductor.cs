﻿using UnityEngine;
using System.Collections;

public class RhythmConductor : MonoBehaviour {
    //enum for the track
    public enum TrackNumber { track1 = 1, track2 = 2};
    public TrackNumber currentTrack = 0;
    
    //song to use
    public AudioSource song;
     
    //time duration of a beat per bar
    public int crotchetsPerBar = 7;
    //beats per minute, or speed of the song
    public double bpm = 60;
    //time duration of a beat
    public double crotchet;
    //speed of the notes going down the track
    public double trackSpeed;
    //position in time of the song
    public double songPosition;
    //offset of the song due to gap at very beginning
    public double offset = 0.1f;

    //positions which the note will start from
    public Vector3 track1StartPos;
    public Vector3 track2StartPos;
    //positions which the note will disappear from
    public Vector3 track1EndPos;
    public Vector3 track2EndPos;

    //Note prefab to spawn
    public GameObject NotePrefab1;
    public GameObject NotePrefab2;
    GameObject note;

    //track to spawn note at
    public int trackNum;


    //call script about song information
    GameObject songObj;
    SongInfoBehavior songInfoScript;

    //pause information
    GameObject pauseButton;
    PauseButtonBehavior pauseScript;

    public bool crRunning = false;

    bool hasPlayedOnce = false;

    public double durationInSamples = 0.0;

    public int i = 0;
    // Use this for initialization
    void Start () {
        songObj = GameObject.FindGameObjectWithTag("Song");
        songInfoScript = songObj.GetComponent<SongInfoBehavior>();
        pauseButton = GameObject.FindGameObjectWithTag("PauseButton");
        pauseScript = pauseButton.GetComponent<PauseButtonBehavior>();

        /**If any of the track positions are not set, set them based on the positions in the heiarchy if they exist **/
        if (track1StartPos == Vector3.zero)
        {
            if (GameObject.Find("NoteSpawner1") != null)
                track1StartPos = GameObject.Find("NoteSpawner1").transform.position;
        }
        if (track2StartPos == Vector3.zero)
        {
            if (GameObject.Find("NoteSpawner2") != null)
                track2StartPos = GameObject.Find("NoteSpawner2").transform.position;
        }
        if (track1EndPos == Vector3.zero)
        {
            if (GameObject.Find("NoteDestroyer1") != null)
                track1EndPos = GameObject.Find("NoteDestroyer1").transform.position;
        }
        if (track2EndPos == Vector3.zero)
        {
            if (GameObject.Find("NoteDestroyer2") != null)
                track2EndPos = GameObject.Find("NoteDestroyer2").transform.position;
        }
        song = GetComponent<AudioSource>();
        if(song != null)
        {
            durationInSamples = song.clip.samples;

        }
        
    }
	
	// Update is called once per frame
	void Update () {

        if (!pauseScript.isPaused)
        {
            trackNum = Random.Range(0, 9);
            if (trackNum < 5)
            {
                currentTrack = TrackNumber.track1;
            }
            else
            {
                currentTrack = TrackNumber.track2;
            }
            if (songInfoScript.isLoaded && song != null)
            {
                bpm = songInfoScript.songTempo;

                //set the speed of the crotchet
                crotchet = 60.0f / bpm;
                trackSpeed = songInfoScript.trackSpeed;
                //print("Before Play");
                if (!song.isPlaying && !hasPlayedOnce)
                    song.Play();


                if (song.isPlaying)
                {
                    hasPlayedOnce = true;
                    //print(song.clip.samples);
                    //set the song position based off of the time samples
                    songPosition = song.timeSamples / 44100.0f - offset;//add -offeset
                    if (i <= songInfoScript.songPositions.Count)
                    {
                        trackNum = Random.Range(0, 9);
                        StartCoroutine(SpawnNote());
                    }

                }
            }
        }
    }

    IEnumerator SpawnNote()
    {
        
        if (i < songInfoScript.songPositions.Count)
        {
            switch (currentTrack)
            {
                case TrackNumber.track1:
                    if (songPosition >= songInfoScript.songPositions[i])
                    {
                        note = Instantiate(NotePrefab1, new Vector3(track1StartPos.x, track1StartPos.y, track1StartPos.z), new Quaternion(0, 0, 0, 0)) as GameObject;
                        StartCoroutine(MoveNote1(note));
                        i++;
                    }
                    break;
                case TrackNumber.track2:
                    if (songPosition >= songInfoScript.songPositions[i])
                    {
                        note = Instantiate(NotePrefab2, new Vector3(track2StartPos.x, track2StartPos.y, track2StartPos.z), new Quaternion(0, 0, 0, 0)) as GameObject;
                        StartCoroutine(MoveNote2(note));
                        i++;
                    }
                    break;
                case 0:
                    print("Error");
                    break;
                default:
                    if (songPosition >= songInfoScript.songPositions[i])
                    {
                        note = Instantiate(NotePrefab1, new Vector3(track1StartPos.x, track1StartPos.y, track1StartPos.z), new Quaternion(0, 0, 0, 0)) as GameObject;
                        StartCoroutine(MoveNote1(note));
                        i++;
                    }
                    break;
            }
            
            
        }
        else
        {
            yield return null;
        }
        
        //now need to subtract if statement with time that it takes to reach the bottom of the track
        //yield return null;
        //crRunning = false;

    }

    IEnumerator MoveNote1(GameObject note_obj)
    {
        if (!pauseScript.isPaused)
        {
            //print("Coroutine 1 started");
            if (note_obj != null)
            {
                while (note_obj != null && Vector2.Distance(note_obj.transform.position, track1EndPos) > 0.05f)
                {
                    while (pauseScript.isPaused)
                    {
                        yield return null;
                    }
                    //print("Hello?");
                    note_obj.transform.position = Vector3.MoveTowards(note_obj.transform.position, track1EndPos, (float)(trackSpeed));//must change to time of song eventually

                    yield return null;
                }
                //yield return null;
                //print("Reached the end of track 1");
                //Destroy(note_obj);
            }
        }
    }

    //Moves the note on track 2 from the start of the track to the end of the track based on the tempo of the song
    IEnumerator MoveNote2(GameObject note_obj)
    {
        if (!pauseScript.isPaused)
        {
            //print("Coroutine 2 started");
            if (note_obj != null)
            {
                while (note_obj != null && Vector2.Distance(note_obj.transform.position, track2EndPos) > 0.05f)
                {
                    while (pauseScript.isPaused)
                    {
                        yield return null;
                    }
                    note_obj.transform.position = Vector3.MoveTowards(note_obj.transform.position, track2EndPos, (float)(trackSpeed));//must change to time of song eventually
                    yield return null;
                }
                //yield return null;
                //print("Reached the end of track 2");
                //Destroy(note_obj);
            }
        }
    }
}
