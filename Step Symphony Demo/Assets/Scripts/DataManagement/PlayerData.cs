﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class PlayerData : MonoBehaviour {
    public static PlayerData playerData;

    public string username;
    private int kyokuboCount;
    public List<Kyokubou> kyokuboCollection;
    private DateTime timeLastUpdated;
    SaveGame saveFile;
    public string saveFilePath;
    public string jsonData;
    public StepSymphonyBackend ssBackend;

    //Create a singleton for the player data
    void Awake() {
        if (playerData == null)
        {
            DontDestroyOnLoad(gameObject);
            playerData = this;
            //kyokuboCollection = new SerializableStringBoolDict();
            kyokuboCollection = new List<Kyokubou>();
            ssBackend = GetComponent<StepSymphonyBackend>();
            ssBackend.OnSaveGameSuccess += OnSaveGameSuccess;
            saveFilePath = Application.persistentDataPath + "/save.json";
        }
        else if (playerData != this)
        {
            Destroy(gameObject);
        }

        
        
	}
	
	// Update is called once per frame
	void Update () {
	}

    /// <summary>
    /// Saves all of the data at this point into a json file located locally, then it uploads that file to the players database
    /// </summary>
    public void SaveProgress()
    {
        saveFile = new SaveGame(username, kyokuboCollection, kyokuboCount, timeLastUpdated);
        //serialize this into a json file
        jsonData = JsonUtility.ToJson(saveFile);

        //store the json file locally
        File.WriteAllText(saveFilePath, jsonData);
        Debug.Log("Wrote successfuly"+ jsonData + "to:" + saveFilePath);


        //string writableData = jsonData;
        //once the file has been stored locally, save it to the db
        if(jsonData != null)
            ssBackend.SaveGame(username, saveFilePath);
        
    }

    /// <summary>
    /// Takes the json string from the database and makes a savegame, with all of the saved infomation
    /// </summary>
    /// <param name="jsonData"></param>
    /// <returns></returns>
    public SaveGame LoadProgress(string jsonData)
    {
        return JsonUtility.FromJson<SaveGame>(jsonData);

    }
    /// <summary>
    /// Should be called after load progress
    /// </summary>
    /// <param name="gameData"></param>
    public void PopulatePlayerData(SaveGame gameData)
    {
        username = gameData.username;
        kyokuboCollection = gameData.kyokubouCollection;
        kyokuboCount = gameData.kyokubouCount;
        timeLastUpdated = gameData.timeLastUpdated;
        
    }

    /// <summary>
    /// This is a helper function to add the kyokubo to the dictionary located within the player data function
    /// </summary>
    /// <param name="newAddition"></param>
    public void AddKyokuboToCollection(string newAdditionName)
    {
        Kyokubou kyoToAdd;
        //iterate through the dictionary and cross reference names
        kyoToAdd = KyokubouData.kyokubouData.GetKyokubo(newAdditionName);
        //maybe check to see first if we can use kyokubo instead of names???
        //foreach()
        if (!kyokuboCollection.Contains(kyoToAdd))
        {
            kyokuboCollection.Add(kyoToAdd);
            kyokuboCount++;
        }
        else
        {
            Debug.Log("You already have this kyokubo!");
        }
         
    }

    public void OnSaveGameSuccess()
    {
        Debug.Log("Game Has Successfully saved to the database");
    }
}
