﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class SerializableDictionary<TKey, TVal> : Dictionary<TKey, TVal>, ISerializationCallbackReceiver {

    //Make a list of keys that can be serialized
    [SerializeField]
    private List<TKey> listOfKeys = new List<TKey>();

    [SerializeField]
    private List<TVal> listOfVals = new List<TVal>();


    //This function will save the dictionary to a list
    public void OnBeforeSerialize()
    {
        listOfKeys.Clear();
        listOfVals.Clear();

        //iterate through the pair of the keys and values and add the right keys and values
        foreach(KeyValuePair<TKey, TVal> pair in this)
        {
            listOfKeys.Add(pair.Key);
            listOfVals.Add(pair.Value);
        }
    }

    //Deserializes the dictionary
    public void OnAfterDeserialize()
    {
        this.Clear();

        if(listOfKeys.Count != listOfVals.Count)
        {
            throw new System.Exception(string.Format("there are {0} keys and {1} values after deserialization, make sure that both key and value types are serializable."));
        }
        for(int i = 0; i < listOfKeys.Count; i++)
        {
            this.Add(listOfKeys[i], listOfVals[i]);
        }
    }
}
