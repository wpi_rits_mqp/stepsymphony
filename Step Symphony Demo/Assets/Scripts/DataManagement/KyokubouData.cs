﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class KyokubouData : MonoBehaviour {

    public static KyokubouData kyokubouData;
    public Dictionary<string, Kyokubou> kyokubouDictionary; //this dictionary makes it possible to get the kyokubo based off of the name
    public StepSymphonyBackend kyoBackend;
    public List<GameObject> listOfKyokubou;
    public GameObject kyokubouPrefab;

    public int current_kyokubou;

    public GameObject currentKyokubou;

    //Create singleton aspect of this class
	void Awake () {
	    if(kyokubouData == null)
        {
            DontDestroyOnLoad(gameObject);
            kyokubouData = this;
            //instantiate any other things here, such as load everything
            kyokubouDictionary = new Dictionary<string, Kyokubou>();

            // listOfKyokubou = new List<Kyokubou>();
            //// listOfKyokubou.Add(TegaScript.tega);
            // listOfKyokubou.Add(KokokoScript.kokoko);
            // listOfKyokubou.Add(YushiyaScript.yushiya);
            //get the kyokubo information from the database
            // if (kyoBackend == null)
            // {
            //    kyoBackend = PlayerData.playerData.ssBackend;
            //Debug.Log("Found the kyokubo backend");
            //    kyoBackend.OnRetrieveInfoSuccess += OnRetrieveInfoSuccess;
            // }
            //Debug.Log("Added delegate method");
            //currentKyokubou = gameObject.AddComponent<Kyokubou>();
            incrementKyokubou();
            currentKyokubou = listOfKyokubou[current_kyokubou];

            




            /*   foreach (Kyokubou kyo in listOfKyokubou)
              {
                  kyokubouDictionary.Add(kyo.kyoname, kyo);
                  Debug.Log("Added kyokubo: " + kyo.kyoname + "to the dictionary");
                  Debug.Log("Kyokubo Reward is:" + kyo.reward);
                  //->>>>>>//listOfKyokubou.Add(kyo);
                  //Instantiate a new kyokubo game object
                  Instantiate(kyokubouPrefab);
                  kyokubouPrefab.gameObject.name = kyo.kyoname;
                  kyokubouPrefab.GetComponent<Kyokubou>().location_one = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().location_one;
                  kyokubouPrefab.GetComponent<Kyokubou>().location_two = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().location_two;
                  kyokubouPrefab.GetComponent<Kyokubou>().location_three = kyo.GetComponent<KyokubouData>().kyokubouPrefab.GetComponent<Kyokubou>().location_three;
                  kyokubouPrefab.GetComponent<Kyokubou>().id = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().id;
                  kyokubouPrefab.GetComponent<Kyokubou>().kyoname = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().kyoname;
                  kyokubouPrefab.GetComponent<Kyokubou>().instrument = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().instrument;
                  kyokubouPrefab.GetComponent<Kyokubou>().image = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().image;
                  kyokubouPrefab.GetComponent<Kyokubou>().music = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().music;
                  kyokubouPrefab.GetComponent<Kyokubou>().description = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().description;
                  kyokubouPrefab.GetComponent<Kyokubou>().reward = kyo.GetComponent<KyokubouData>().GetComponent<Kyokubou>().reward;
                  //DontDestroyOnLoad(kyokubouPrefab);
              }*/

            //fill the information recieved into the dictionary(LOCAL VERSION)
            // FillKyokuboDictionary();
        }
        else if(kyokubouData != this)
        {
            Destroy(gameObject);
        }
	}
    void Start()
    {
        //kyoBackend.RetrieveInfo();
        Debug.Log("Retrieving info has been started");

        Instantiate(kyokubouPrefab);
       // kyokubouPrefab.gameObject.name = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().kyoname;
       // kyokubouPrefab.GetComponent<Kyokubou>().location_one = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().location_one;
      //  kyokubouPrefab.GetComponent<Kyokubou>().location_two = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().location_two;
      //  kyokubouPrefab.GetComponent<Kyokubou>().location_three = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().location_three;
      //  kyokubouPrefab.GetComponent<Kyokubou>().id = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().id;
      //  kyokubouPrefab.GetComponent<Kyokubou>().kyoname = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().kyoname;
     //   kyokubouPrefab.GetComponent<Kyokubou>().instrument = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().instrument;
      //  kyokubouPrefab.GetComponent<Kyokubou>().image = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().image;
      //  kyokubouPrefab.GetComponent<Kyokubou>().music = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().music;
     //   kyokubouPrefab.GetComponent<Kyokubou>().description = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().description;
     //   kyokubouPrefab.GetComponent<Kyokubou>().reward = listOfKyokubou[current_kyokubou].GetComponent<Kyokubou>().reward;


    }

    public void incrementKyokubou()
    {
        current_kyokubou = current_kyokubou + 1;
    }

    public void OnRetrieveInfoSuccess(List<Kyokubou> kyokuboInfo)  
    {
        //WRITE A FUNCTION TO PARSE JSON INFORMNATION FROM DB AND FILL DICTIONARY
           foreach(Kyokubou kyo in kyokuboInfo)
       // foreach (Kyokubou kyo in listOfKyokubou)
        {
            kyokubouDictionary.Add(kyo.kyoname, kyo);
            Debug.Log("Added kyokubo: " + kyo.kyoname + "to the dictionary");
            Debug.Log("Kyokubo Reward is:" + kyo.reward);
            //->>>>>>//listOfKyokubou.Add(kyo);
            //Instantiate a new kyokubo game object
            Instantiate(kyokubouPrefab);
            kyokubouPrefab.gameObject.name = kyo.kyoname;
            kyokubouPrefab.GetComponent<Kyokubou>().location_one = kyo.location_one;
            kyokubouPrefab.GetComponent<Kyokubou>().location_two = kyo.location_two;
            kyokubouPrefab.GetComponent<Kyokubou>().location_three = kyo.location_three;
            kyokubouPrefab.GetComponent<Kyokubou>().id = kyo.id;
            kyokubouPrefab.GetComponent<Kyokubou>().kyoname = kyo.kyoname;
            kyokubouPrefab.GetComponent<Kyokubou>().instrument = kyo.instrument;
            kyokubouPrefab.GetComponent<Kyokubou>().image = kyo.image;
            kyokubouPrefab.GetComponent<Kyokubou>().music = kyo.music;
            kyokubouPrefab.GetComponent<Kyokubou>().description = kyo.description;
            kyokubouPrefab.GetComponent<Kyokubou>().reward = kyo.reward;
            //DontDestroyOnLoad(kyokubouPrefab);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="kyokuboInfo"></param>
    void ParseKyokubouInformation(string kyokuboInfo)
    {
        List<Kyokubou> kyoList = new List<Kyokubou>();

    }
    

    /// <summary>
    /// This will make a call to the kyokubo database and fill the dictionary with the name and kyokubo object
    /// Temporarialy will manually fill the kdictionary locally ***CHANGE THIS LATER!
    /// </summary>
    void FillKyokuboDictionary()
    {
        kyokubouDictionary.Add(AiScript.ai.kyoname, AiScript.ai);
        kyokubouDictionary.Add(HiiScript.hii.kyoname, HiiScript.hii);
        kyokubouDictionary.Add(InokozaScript.inokoza.kyoname, InokozaScript.inokoza);
        kyokubouDictionary.Add(KataakiScript.kataaki.kyoname, KataakiScript.kataaki);
        kyokubouDictionary.Add(KokokoScript.kokoko.kyoname, KokokoScript.kokoko);
        kyokubouDictionary.Add(KomiyaScript.komiya.kyoname, KomiyaScript.komiya);
        kyokubouDictionary.Add(KumagamineScript.kumagamine.kyoname, KumagamineScript.kumagamine);
        kyokubouDictionary.Add(OsachiScript.osachi.kyoname, OsachiScript.osachi);
        kyokubouDictionary.Add(RingoScript.ringo.kyoname, RingoScript.ringo);
        kyokubouDictionary.Add(ShoujiScript.shouji.kyoname, ShoujiScript.shouji);
        kyokubouDictionary.Add(TegaScript.tega.kyoname, TegaScript.tega);
        kyokubouDictionary.Add(TomaruScript.tomaru.kyoname, TomaruScript.tomaru);
        kyokubouDictionary.Add(UshikyuuScript.ushikyuu.kyoname, UshikyuuScript.ushikyuu);
        kyokubouDictionary.Add(YushiyaScript.yushiya.kyoname, YushiyaScript.yushiya);

    }


    /// <summary>
    /// Given the name of the kyokubou, it will go through the keys in the dictionary and
    /// will return the kyokubo of the given key
    /// </summary>
    /// <param name="kyoname"></param>
    /// <returns></returns>
    public Kyokubou GetKyokubo(string kyoname)
    {
        Kyokubou kyoToGet;
        //Checks to see if the name matches any key within the dictionary
        if (kyokubouDictionary != null && kyokubouDictionary.ContainsKey(kyoname))
        {
            kyoToGet = kyokubouDictionary[kyoname];
            return kyoToGet;
        }
        else
        {
            Debug.Log("Could not find the kyokubo in the database");
            return null;
        }
        
    }
	
}
