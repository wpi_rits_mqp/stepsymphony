﻿using System;
using System.Collections.Generic;
/*
 * Author: Marco Duran
 * Describes the criteria to save to the database
 * Will save in format that is recognized in the database(JSON)
 */
[Serializable]
public class SaveGame {

    public string username;
    public List<Kyokubou> kyokubouCollection;
    public int kyokubouCount;
    public DateTime timeLastUpdated;

    public SaveGame(string username, List<Kyokubou> collection, int count, DateTime lastUpdated)
    {
        this.username = username;
        this.kyokubouCollection = collection;
        this.kyokubouCount = count;
        this.timeLastUpdated = lastUpdated;
    }

}
