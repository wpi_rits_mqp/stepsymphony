﻿using UnityEngine;
using System.Collections;

public class OpenMenuPanel : MonoBehaviour {
    public bool isMenuOpen = false;
    public GameObject menuPanel;

    void Awake()
    {
        menuPanel = GameObject.FindGameObjectWithTag("MenuPanel");
        if (!isMenuOpen)
        {
            menuPanel.SetActive(false);
        }
    }

    public void TogglePanel()
    {
        if(menuPanel != null)
        {
            if (!isMenuOpen && !menuPanel.activeInHierarchy)
            {
                menuPanel.SetActive(true);
                isMenuOpen = true;
            }
            else if (isMenuOpen && menuPanel.activeInHierarchy)
            {
                menuPanel.SetActive(false);
                isMenuOpen = false;
            }
        }
        
    }
}
