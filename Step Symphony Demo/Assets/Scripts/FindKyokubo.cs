﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FindKyokubo : MonoBehaviour
{
    //Holds player's facing direction (0 - 360), Kyokubou's facing direction (0 - 360), and the difference between the two (-180 - +180)
    public double kyokubo_angle, player_compass_angle, difference_angle;

    //Holds current audio piece used
    public AudioSource audio1;

    //Holds direction to pan audio (-1 - +1) and volume to set audio (0 - 1)
    public double set_pan, set_volume;

    //Holds player's latitude and longitude
    public double lat, lon;

    //Used to access and hold the kyokubo's stored latitude and longitude
    public GameObject kyokubo_obj;
    public double kyokubo_lat, kyokubo_lon;

    //Holds number of feet in 1 degree of latitude and longitude for the Kyoto area.
    public double lat_degree_in_feet, lon_degree_in_feet;

    //Holds the absolute distance between the player and Kyokubou.
    public double abs_distance_kyokubo;

    //Holds, in feet, the radius of the zone.
    public double zone_size;

    //Button to enter AR mode, only appears on screen when Kyokubou is within range
    public Button ar_button;

    //Used to set the current direction the Kyokubou is from the player
    public enum CompassDirection
    {
        n = 1,
        ne = 2,
        e = 3,
        se = 4,
        s = 5,
        sw = 6,
        w = 7,
        nw = 8
    };

    public CompassDirection current_dir = 0;

    //Player sprite
    public GameObject player;

   // public double previous_compass_angle;

    //FOR THE DEMO
    public int currentKyokubou;
    public Sprite kyokubouSprite;

    // Use this for initialization
    public void Start ()
    {
        //Set the frame rate to 60 frames per second

        //Start Compas services
        if(Input.compass.enabled == false)
        {
            Input.compass.enabled = true;
        }

        //Start Location services
        //StartLocationServices();
        //Input.location.Start();

        //Number of feet for each degree of latitude and longitude in the Rakuchu region.
        lat_degree_in_feet = 363977.46;
        lon_degree_in_feet = 299501.16;

        //Get Kyokubou's latitude and longitude to find kyokubo angle later

        kyokubo_lat = 42.27412;
        kyokubo_lon = -71.80841;


        player = GameObject.FindGameObjectWithTag("Player");

      //  previous_compass_angle = 0;
        //Set the ar_button
        ar_button = GameObject.FindGameObjectWithTag("ar").GetComponent<Button>();
        ar_button.gameObject.SetActive(false);
        ar_button.enabled = false;

        //Set the zone's radius to 500 feet
        zone_size = 1000;  //Change this value when testing directional sound.

        //Start playing audio
        audio1 = GameObject.FindGameObjectWithTag("locateaudio").GetComponent<AudioSource>();
        audio1.Play();
    }




	
	// Update is called once per frame
	void Update ()
    {
        //Get the player's location using latitude and longitude
        lat = Input.location.lastData.latitude;
        lon = Input.location.lastData.longitude;

        //Get the player's compass heading and set it is a double
        //previous_compass_angle = player_compass_angle;
        player_compass_angle = Input.compass.magneticHeading;

        //Find which CompassDirection for the Kyokubo_obj
        findKyokuboDirection(kyokubo_lat, kyokubo_lon, lat, lon);

        //Set the angle direction for kyokubo_angle
        findKyokuboAngle(current_dir);

        //Find the difference between the Kyokubou's and Player's direction
        findDifferenceAngle(kyokubo_angle, player_compass_angle);

        //Find a number between -1 and 1 to set the audio pan to
        findPan(difference_angle);
        //Fine a number between 0 and 1 to set the audio volume to
        findVolume(abs_distance_kyokubo);

        //Set new pan and volume
        changePan(set_pan);
        changeVolume(set_volume);

        //player.transform.Rotate(0, 0, (float) (player_compass_angle - previous_compass_angle));

        //When the player is close enough to the Kyokubou, enable Augmented Reality
        if (abs_distance_kyokubo <= 200)//change value back to 50
        {
            ar_button.gameObject.SetActive(true);
            ar_button.enabled = true;
            audio1.panStereo = 0;
            audio1.volume = 1;
        }
        else
        {
            ar_button.gameObject.SetActive(false);
            ar_button.enabled = false;
        }
    }

    /*
     * findDifferenceAngle
     * Takes in: double angle_kyokubo, double angle_player
     * Returns: double
     * Finds the angle difference between the player and kyokubo
     */
    public double findDifferenceAngle(double angle_kyokubo, double angle_player)
    {
        //Add 360 to both angle, then find the difference
        //The 360 added is to fix the left panning bug
        angle_kyokubo = angle_kyokubo + 360;
        angle_player = angle_player + 360;
        difference_angle = angle_kyokubo - angle_player;

        if (difference_angle > 180)
        {
            difference_angle = difference_angle - 360;
        }

        else if(difference_angle < -180)
        {
            difference_angle = difference_angle + 360;
        }

        return difference_angle;
    }

    /*
    * findKyokuboDirection
    * Takes in: double kyo_lat, double kyo_lon, double player_lat, double player_lon
    * Returns: CompassDirection
    * Finds difference in lat and lon between player and kyokubo, and sets a direction the Kyokubou is in
    */
    public CompassDirection findKyokuboDirection(double kyo_lat, double kyo_lon, double player_lat, double player_lon)
    {
        //The difference between player and Kyokubou, 
        double difference_lat = kyokubo_lat - player_lat;
        double difference_lon = kyokubo_lon - player_lon;

        //Find absolute distance between player and Kyokubou
        findAbsDistance(difference_lat, difference_lon);

        //Find compass direction players needs to face to walk toward Kyokubou (NEEDS REFINING)
        if (difference_lat < 0 && difference_lon == 0)
        { //South
            current_dir = CompassDirection.s;
            return current_dir;
        }

        else if (difference_lat < 0 && difference_lon < 0)
        { //SouthWest
            current_dir = CompassDirection.sw;
            return current_dir;
        }

        else if (difference_lat == 0 && difference_lon < 0)
        { //West
            current_dir = CompassDirection.w;
            return current_dir;
        }

        else if (difference_lat > 0 && difference_lon < 0)
        { //NorthWest
            current_dir = CompassDirection.nw;
            return current_dir;
        }

        else if (difference_lat > 0 && difference_lon == 0)
        { //North
            current_dir = CompassDirection.n;
            return current_dir;
        }

        else if (difference_lat > 0 && difference_lon > 0)
        { //NorthEast
            current_dir = CompassDirection.ne;
            return current_dir;
        }

        else if (difference_lat == 0 && difference_lon > 0)
        { //East
            current_dir = CompassDirection.e;
            return current_dir;
        }

        else if (difference_lat < 0 && difference_lon < 0)
        { //SouthEast
            current_dir = CompassDirection.se;
            return current_dir;
        }

        else
        { //ERROR
            current_dir = 0;
            return current_dir;
        }
    }

    /*
    * findKyokuboAngle
    * Takes in: CompassDirection
    * Returns: double
    * Based on input CompassDirection, set Kyokubou's angle (0 - 315)
    * NEEDS REFINING
    */
    public double findKyokuboAngle(CompassDirection dir)
    {
        switch (dir)
        {
            case CompassDirection.n: //north
                kyokubo_angle = 0;
                break;

            case CompassDirection.ne: //northeast
                kyokubo_angle = 45;
                break;

            case CompassDirection.e: //east
                kyokubo_angle = 90;
                break;

            case CompassDirection.se: //southeast
                kyokubo_angle = 135;
                break;

            case CompassDirection.s: //south
                kyokubo_angle = 180;
                break;

            case CompassDirection.sw: //southwest
                kyokubo_angle = 225;
                break;

            case CompassDirection.w: //west
                kyokubo_angle = 270;
                break;

            case CompassDirection.nw: //northwest
                kyokubo_angle = 315;
                break;
        }
        return kyokubo_angle;
    }

    /*
     * findAbsDistance
     * Takes in: double lat_dif, double lon_dif
     * Returns: double
     * Finds the absolute distance between player and kyokubo, based on the difference in latitude and longitude
     */
    public double findAbsDistance(double lat_dif, double lon_dif)
    {
        //Convert to feet
        double lat_dif_feet = lat_dif * lat_degree_in_feet;
        double lon_dif_feet = lon_dif * lon_degree_in_feet;

        //Square them and add them together
        lat_dif_feet = lat_dif_feet * lat_dif_feet;
        lon_dif_feet = lon_dif_feet * lon_dif_feet;
        abs_distance_kyokubo = lat_dif_feet + lon_dif_feet;

        //Now, take the square root of abs_distance_kyokubo
        abs_distance_kyokubo = Mathf.Sqrt((float)abs_distance_kyokubo);

        return abs_distance_kyokubo;
    }

    /*
     * findVolume
     * Takes in: double abs_dist
     * Returns: double
     * Finds the correct volume to play the audio at.
     */
    public double findVolume(double abs_dist)
    {
        //If player is not inside the zone
        if (abs_dist >= zone_size)
        {
            //Set the audio volume to 0;
            set_volume = 0;
        }

        else
        {
            //The closer the player is to the Kyokubou, the louder the music
            set_volume = (1 - abs_dist / zone_size);
        }
        return set_volume;
    }

    /*
     * findPan
     * Takes in: double difference
     * Returns: double
     * Finds the number to pan the audio to based on the angle difference
     */
    public double findPan(double difference)
    {
        //If the player is facing the Kyokubou
        if (difference <= 20 && difference >= -20)
        {
            //audio plays in both ears
            set_pan = 0;
        }

        else
        {
            set_pan = (difference / 180);
        }
        return set_pan;
    }

    //Set the new pan for the audio
    public void changePan(double pan)
    {
        audio1.panStereo = (float)pan;
    }

    //Set the new volume for the audio
    public void changeVolume(double volume)
    {
        audio1.volume = (float)volume;
    }
}

