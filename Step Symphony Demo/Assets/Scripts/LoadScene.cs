﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public string scene = "";
    public string asyncLoadingScene = "";
    public bool isLoadingAsync = false;

    public void Load()
    {
        if (isLoadingAsync && asyncLoadingScene.Length > 0)
        {
            PlayerPrefs.SetString("SceneToLoad", asyncLoadingScene);
            SceneManager.LoadScene(scene);
        }
        else
            SceneManager.LoadScene(scene);
    }

}
