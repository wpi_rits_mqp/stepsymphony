﻿using UnityEngine;
using System.Collections;

public class Overlay_BackCamTexture : MonoBehaviour {
    public WebCamTexture webCamTexture;
    public float height;
    public float width;

    // Use this for initialization
    void Start () {
        //scale the size of the display board to the size of the screen
        height = Camera.main.orthographicSize * 3.7f;
        width = height * Screen.width / Screen.height;
        transform.localScale = new Vector3(width, height, 1); 
        //start the webcam 
        webCamTexture = new WebCamTexture();
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webCamTexture;
        if(webCamTexture != null)
            webCamTexture.Play();
	}

    void OnDestroy()
    {
        webCamTexture.Stop();
    }
	
	// Update is called once per frame
	void Update () {
        
	}
}
