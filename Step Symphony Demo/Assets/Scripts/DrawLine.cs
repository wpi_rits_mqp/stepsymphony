﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent (typeof(LineRenderer))]
public class DrawLine : MonoBehaviour
{

    private LineRenderer line;
    public bool isScreenTouched;
    public List<Vector3> pointsList;
    private Vector3 touchPos;

    //Game object for drawing box 
    public GameObject draw_box;
    RaycastHit2D touchInfo;

    //GameObject for shape
    public GameObject shape;

    //Collider for start point of trace shape
    public GameObject start_obj;
    public CircleCollider2D start_point;
    //Collider for shape boundary
    //public PolygonCollider2D shape_boundary;

    //bool to check if the player has started on the start point
    public bool startedCorrectly;

    //Start point parameters, to recreate once destroyed
    public float start_radius;
    public Vector3 start_pos;
    public Sprite start_sprite;


    struct myLine
    {
        public Vector3 start;
        public Vector3 end;
    };

	// Use this for initialization
	void Start ()
    {
        //Create a line renderer component
        line = GetComponent<LineRenderer>();
        //line.material = new Material(Shader.Find("Particles/VertexLit Blended"));
        line.SetVertexCount(0);
        line.SetWidth(0.3f, 0.3f);
        line.SetColors(Color.green, Color.green);
        line.useWorldSpace = true;
        isScreenTouched = false;
        pointsList = new List<Vector3>();

        //Initialize draw box 
        draw_box = GameObject.FindGameObjectWithTag("DrawBox");

        //Initialize trace shape
        shape = GameObject.FindGameObjectWithTag("Shape");

        //initialize start point
        start_obj = GameObject.Find("StartPoint");

        if(start_obj != null)
        {
            start_point = start_obj.GetComponent<CircleCollider2D>();
        }

        startedCorrectly = false;
	}
	
    
	// Update is called once per frame
	void Update ()
    {
        //if you are touching the screen and only one finger is touching the screen, remove old line and set its color to green
        if(Input.touchCount > 0 && Input.touchCount < 2)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                isScreenTouched = true;
                line.SetVertexCount(0);
                pointsList.RemoveRange(0, pointsList.Count);
                line.SetColors(Color.green, Color.green);
            }
            //If the finger was removed from the screen, is touched is false
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                isScreenTouched = false;
            }
        }
        //Drawing line when the finger is moving along the screen
        if (isScreenTouched)
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            touchPos.z = -5;
            //Convert the Vector3 to a vector 2
            Vector2 touchPos2D = new Vector2(touchPos.x, touchPos.y);
            //Get Raycast info based on where you touched the screen
            touchInfo = Physics2D.Raycast(touchPos2D, Camera.main.transform.forward);
            //check to see if you started correctly
            if (touchInfo.collider == start_point)
            {
                startedCorrectly = true;
                start_obj.SetActive(false);
            }

            if (!pointsList.Contains(touchPos))
            {
                if(touchInfo.collider != null)
                {
                    //print(touchInfo.transform.tag);
                    //Check to see if you are touching the shape and if you started correctly
                    if (startedCorrectly)
                    {
                        //Send another raycast out
                        touchInfo = Physics2D.Raycast(touchPos2D, Camera.main.transform.forward);
                        touchInfo = Physics2D.Raycast(touchPos2D, Camera.main.transform.forward);
                        touchInfo = Physics2D.Raycast(touchPos2D, Camera.main.transform.forward);
                        //if (touchInfo.collider != shape_boundary)
                        //{
                            pointsList.Add(touchPos);
                            line.SetVertexCount(pointsList.Count);
                            line.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);
                            if (isLineCollide())
                            {
                                isScreenTouched = false;
                                line.SetColors(Color.red, Color.red);
                            }
                        //}
                        
                    }
                }  
            }
        }
        //When you remove your finger from the screen, delete the lines from the screen
        if(!isScreenTouched)
        {
            line.SetVertexCount(0);
            pointsList.RemoveRange(0, pointsList.Count);
            line.SetColors(Color.green, Color.green);
            startedCorrectly = false;
            //recreate the start point if there is no circle collider (since it would be destroyed)
            if(start_obj.activeInHierarchy == false)
            {
                start_obj.SetActive(true);
            }
        } 
	}

    
    /*Function that checks to see if the current
      line (line drawn by last 2 points)
      has collided with the line
    */
    
    bool isLineCollide()
    {
        if (pointsList.Count < 2)
            return false;
        int tot_lines = pointsList.Count - 1;
        myLine[] lines = new myLine[tot_lines];
        //If the total amount of lines is greater than one, add them to the points list
        if(tot_lines > 1)
        {
            for(int i = 0; i < tot_lines; i++)
            {
                lines[i].start = (Vector3)pointsList[i];
                lines[i].end = (Vector3)pointsList[i + 1];
            }
        }
        //Go through all of the total lines and check if they intersect with one another
        for (int i = 0; i < tot_lines-1; i++)
        {
            myLine curr_line;
            curr_line.start = (Vector3)pointsList[pointsList.Count - 2];
            curr_line.end = (Vector3)pointsList[pointsList.Count -1];
            if (isLineIntersect(lines[i], curr_line))
                return true;
        }

        return false;
    }

    //Sees if two points are the same
    bool checkPoints(Vector3 start, Vector3 end)
    {
        return start.x == end.x && start.y == end.y;
    }

    //Checks to see if two lines are intersecting with one another
    bool isLineIntersect(myLine l1, myLine l2)
    {
        if (checkPoints(l1.start, l2.start) ||
            checkPoints(l1.start, l2.end) ||
            checkPoints(l1.end, l2.start) ||
            checkPoints(l1.end, l2.end))
        {
            return false;
        }

        return ((Mathf.Max(l1.start.x, l1.end.x) >= Mathf.Min(l2.start.x, l2.end.x)) &&
                (Mathf.Max(l2.start.x, l2.end.x) >= Mathf.Min(l1.start.x, l1.end.x)) &&
                (Mathf.Max(l1.start.y, l1.end.y) >= Mathf.Min(l2.start.y, l2.end.y)) &&
                (Mathf.Max(l2.start.y, l2.end.y) >= Mathf.Min(l1.start.y, l1.end.y))
                ); 
    }
}
