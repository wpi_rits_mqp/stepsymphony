﻿using UnityEngine;
using System.Collections;

public class PlayJingle : MonoBehaviour
{
    public AudioSource audio1;

    public void Play()
    {
        //Start playing audio
        audio1 = GameObject.FindGameObjectWithTag("locateaudio").GetComponent<AudioSource>();
        audio1.Play();
    }
	

}
