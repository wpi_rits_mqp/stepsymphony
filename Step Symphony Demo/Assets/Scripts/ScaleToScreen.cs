﻿using UnityEngine;
using System.Collections;

public class ScaleToScreen : MonoBehaviour {
    SpriteRenderer sr;
    float sprite_width;
    float sprite_height;
    float world_width;
    float world_height;

    public float scale_height = 1.2f;
    public float scale_width = 2 * 1.2f;


	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();

        if (sr == null)
            return;
        if(sr.sprite != null)
        {
            sprite_width = sr.sprite.bounds.size.x;
            sprite_height = sr.sprite.bounds.size.y;

            world_height = Camera.main.orthographicSize / scale_height;
            world_width = (world_height / Screen.height * Screen.width) * scale_width;

            transform.localScale = new Vector3(world_width / sprite_width, world_height / sprite_height, 1);
        }
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
