﻿using UnityEngine;
using System.Collections;

public class SetSortingLayer : MonoBehaviour {
    public Renderer render;
    public string sorting_layer;
    public int sorting_order;
	// Use this for initialization
	void Start () {
	    if(render == null)
        {
            render = this.GetComponent<Renderer>();
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if(render == null)
        {
            render = this.GetComponent<Renderer>();
        }
        render.sortingLayerName = sorting_layer;
        render.sortingOrder = sorting_order;
	}
}
