﻿using UnityEngine;
using System.Collections;

public class CaptureProgress : MonoBehaviour {
    public int captureScore;
    public bool captureSuccess;
    public bool captureFailure;

    GameObject ui;
    Canvas uiCanvas;
    GameObject failurePanel;
    GameObject successPanel;
    GameObject collectionsButton;
    GameObject mapButton;


    // Use this for initialization
    void Start()
    {
        captureScore = 0;
        captureSuccess = false;
        captureFailure = false;

        ui = GameObject.Find("Canvas");

        //if (ui.activeInHierarchy)
            //ui.SetActive(false);

        failurePanel = GameObject.Find("FailurePanel");
        successPanel = GameObject.Find("SuccessPanel");
        collectionsButton = GameObject.Find("CollectionsButton");
        mapButton = GameObject.Find("BackToMapButton");
        

        //make sure all panels are inactive at the start
        failurePanel.gameObject.SetActive(false);
        successPanel.gameObject.SetActive(false);
        collectionsButton.gameObject.SetActive(false);
        mapButton.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        CheckProgress();
    }

    void CheckProgress()
    {
        if (!captureFailure && !captureSuccess)
        {
            if (captureScore == 3)
            {
                captureSuccess = true;
            }
            if (captureScore == -5)
            {
                captureFailure = true;
            }
        }
        if (captureSuccess && !captureFailure)
        {
            ui.SetActive(true);
            //set the success and all buttons active
            successPanel.gameObject.SetActive(true);
            collectionsButton.gameObject.SetActive(true);
            mapButton.gameObject.SetActive(true);
        }
        if (captureFailure && !captureSuccess)
        {
            ui.SetActive(true);
            failurePanel.gameObject.SetActive(true);
            mapButton.gameObject.SetActive(true);
        }
    }
}
