﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

/// <summary>
/// This class contains the serializable data for the kyokubo
/// </summary>
[Serializable]
public class Kyokubou : MonoBehaviour {
    [SerializeField]
    public int id;
    [SerializeField]
    public string kyoname, instrument, image, music, description, reward;
    [SerializeField]
    public Location location_one, location_two, location_three;
    [SerializeField]
    private string jsonData;



    public Kyokubou(Location location_one = null, Location location_two = null,
                    Location location_three = null, int id = 0, string kyoname = "",
                    string instrument = "", string image = "", string music = "",
                    string description = "", string reward = "")
    {
        this.location_one = location_one;
        this.location_two = location_two;
        this.location_three = location_three;
        this.id = id;
        this.kyoname = kyoname;
        this.instrument = instrument;
        this.image = image;
        this.music = music;
        this.description = description;
        this.reward = reward;
        
    }

    /// <summary>
    /// Given the name of the kyokubo, access the kyokubo database and recieve the JSON file
    /// </summary>
    /// <param name="kyoname"></param>
    /// <returns></returns>
    public string GetJsonData(string kyoname)
    {

        return jsonData;
    }

    /// <summary>
    /// Takes the json data and fills in the kyokubo information based of of the string of json data
    /// </summary>
    /// <param name="jsonData"></param>
    public void DeserializeInfo(string jsonData)
    {
        //add instrument

        //add audio file path(for collection music)

        //add description


        //Add locations
        AddLocations(jsonData);

        //once this is all loaded up, add the kyokubo to the dictionary of kyokubos
        AddToDictionary();
    }
    
    /// <summary>
    /// This function takes the JSON data from the database and adds the locations to the list of locations
    /// </summary>
    /// <param name="jsonData"></param>
    public void AddLocations(string jsonData)
    {
        //first get the name of the kyokubo and compare it to the current one

        //once the name has been aquired, look at the locations and iterate through the locations and add them to the list of locations

        //make sure that the amount of locations is not empty

        //give me an indication when it is finished
    }

    /// <summary>
    /// Populates the kyokubo dictionary on spawn, this is to be able to create an infinite amount of kyokubo in the game
    /// </summary>
    public void AddToDictionary()
    {
        //PlayerData.playerData.kyokuboCollection.Add(kyoname, false);
    }
}
