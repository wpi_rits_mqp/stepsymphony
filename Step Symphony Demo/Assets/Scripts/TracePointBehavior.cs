﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
public class TracePointBehavior : MonoBehaviour, IDragHandler
{
    //int for order number
    public int orderNum = 0;

    //bool to check if it was hit
    public bool isHit;

    //circle collider on the object
    public CircleCollider2D circleCollider;

    //Raycast hit
    RaycastHit2D tpHit;
    RaycastHit2D[] listOfHits;

    Vector2 fingerPos;

	// Use this for initialization
	void Start () {
        //Initialize circle collider
        circleCollider = GetComponent<CircleCollider2D>();
        isHit = false;

	}
	
    public void OnDrag(PointerEventData finger)
    {
        print(finger.pointerCurrentRaycast.worldPosition);
            if(finger.pointerCurrentRaycast.worldPosition == transform.position)
            {
                isHit = true;
            }
    }


	// Update is called once per frame
	void Update () {
	    if(Input.touchCount > 0 && Input.touchCount < 2)
        {
            fingerPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            tpHit = Physics2D.Raycast(fingerPos, Camera.main.transform.forward);


            if(tpHit.collider == circleCollider)
            {
                isHit = true;
            }

            if(Input.GetTouch(0).phase == TouchPhase.Began)
            {
                isHit = false;
            }
        }
	}
}
