﻿using UnityEngine;
using System.Collections;

public class ARButtonStopMusic : MonoBehaviour
{
    //Holds current audio piece used
    public AudioSource audio1;

    public void onClick()
    {
        //Stop playing audio
        audio1 = GameObject.FindGameObjectWithTag("locateaudio").GetComponent<AudioSource>();
        audio1.Stop();
    }
}
