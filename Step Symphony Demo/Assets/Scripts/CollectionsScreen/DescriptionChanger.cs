﻿using UnityEngine;
using System.Collections;

public class DescriptionChanger : MonoBehaviour {
    public GameObject collectionButton;

    void OnTriggerStay2D(Collider2D other)
    {
        if(collectionButton != null)
        {
            if(other.CompareTag("CollectionButton"))
                collectionButton.GetComponent<CollectionButton>().hitDescTrigger = true;
        }
    }
}
