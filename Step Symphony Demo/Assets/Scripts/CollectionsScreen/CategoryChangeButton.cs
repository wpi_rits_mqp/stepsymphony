﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CategoryChangeButton : MonoBehaviour {
    public enum InstrumentCategory { shamisen, shinobue };
    public InstrumentCategory currCategory = InstrumentCategory.shamisen;
    public delegate void ChangeInstrumentCategory(InstrumentCategory newCategory);
    public ChangeInstrumentCategory OnChangeInstrumentCategory;

    public Text categoryText;

	// Use this for initialization
	void Awake () {
        categoryText = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        switch (currCategory)
        {
            case InstrumentCategory.shamisen:
                categoryText.text = "Shamisen";
                break;

            case InstrumentCategory.shinobue:
                categoryText.text = "Shinobue";
                break;
        }
	}

    public void ChangeCategory()
    {
        switch(currCategory)
        {
            case InstrumentCategory.shamisen:
                currCategory = InstrumentCategory.shinobue;
                if(OnChangeInstrumentCategory != null)
                    OnChangeInstrumentCategory(currCategory);
                //SendMessage("ChangeCategory", currCategory);
                break;

            case InstrumentCategory.shinobue:
                currCategory = InstrumentCategory.shamisen;
                if (OnChangeInstrumentCategory != null)
                    OnChangeInstrumentCategory(currCategory);
                //SendMessage("ChangeCategory", currCategory);
                break;
        }
    }
}
