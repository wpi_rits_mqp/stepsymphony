﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class CollectionButton : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    //public enum InstrumentCategory { shamisen, shinobue };
    public int buttonNum = 0;
    public string kyoName;//set this
    public bool isUnlockedShamisen = false;
    public bool isUnlockedShinobue = false;
    public bool isBeingDragged = false;
    public bool hitDescTrigger = false;
    public CategoryChangeButton.InstrumentCategory instrumentType = CategoryChangeButton.InstrumentCategory.shamisen;

    public Image unlockedKyoImage;
    public Image unlockedKyoImage2;
    public Text lockedKyoText;
    public GameObject categoryButton;

    public string rewardStr = "";
    public AudioClip rewardClip1;//Shamisen
    public AudioClip rewardClip2;//Shinobue
    public AudioSource shamisenSource;
    public AudioSource shinobueSource;
    public AudioSource musicSource;
    public string description = "";

    public List<Kyokubou> playerCollection;

    int prevButtonNumber;

    public GameObject draggableAreaObj;
    RectTransform draggableArea;
    public Vector3 startPos;
    public float minX, maxX, minY, maxY;

    //delegate method for changing the description text
    public delegate void ChangeDescription(string newDescription, CollectionButton buttonDragged);
    public ChangeDescription OnDescriptionChange;

    //public Vector3 minPosition;
    //public Vector3 maxPosition;

    // Use this for initialization
    void Awake () {
        categoryButton = GameObject.FindGameObjectWithTag("CategoryButton");
        if(categoryButton != null)
        {
            categoryButton.GetComponent<CategoryChangeButton>().OnChangeInstrumentCategory += ChangeInstrumentCategory;

        }
        if(draggableAreaObj != null)
        {
            draggableArea = draggableAreaObj.GetComponent<RectTransform>();
            
        }
        musicSource = GetComponent<AudioSource>();
        shamisenSource = GetComponent<AudioSource>();
        shinobueSource = GetComponent<AudioSource>();
        //rewardClip1 = Resources.Load<AudioClip>(rewardStr);
        //musicSource.clip = rewardClip1;
        //shamisenSource = Component
        shamisenSource.clip = rewardClip1;
        shinobueSource.clip = rewardClip2;

        CheckCurrentCategory();
        //unlockedKyoImage = GetComponentInChildren<Image>(true);
        //lockedKyoText = GetComponentInParent<Text>();
        //check to see if both kyokubo images are not null
        if (!isUnlockedShamisen && !isUnlockedShinobue && unlockedKyoImage != null && unlockedKyoImage2 != null && lockedKyoText != null)
        {
            unlockedKyoImage.enabled = false;
            unlockedKyoImage2.enabled = false;
            lockedKyoText.text = "?";
        }
        //check to see if a specific cateegory is unlocked
        switch (instrumentType)
        {
            case CategoryChangeButton.InstrumentCategory.shamisen:
                if(isUnlockedShamisen && unlockedKyoImage != null && unlockedKyoImage2 != null && lockedKyoText != null)
                {
                    unlockedKyoImage.enabled = true;
                    unlockedKyoImage2.enabled = false;
                    lockedKyoText.text = "";
                }
                break;
            case CategoryChangeButton.InstrumentCategory.shinobue:
                if (isUnlockedShinobue && unlockedKyoImage != null && unlockedKyoImage2 != null && lockedKyoText != null)
                {
                    unlockedKyoImage.enabled = false;
                    unlockedKyoImage2.enabled = true;
                    lockedKyoText.text = "";
                }
                break;

        }
        

        startPos = gameObject.GetComponent<RectTransform>().localPosition;
            
	}

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(!isBeingDragged)
            isBeingDragged = true;
        //draggableArea = draggableAreaObj.GetComponent<RectTransform>();
        //SetDraggedPosition(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        //check to see if you have touched the description trigger
       
        SetDraggedPosition(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //return the button back to its original position
        gameObject.GetComponent<RectTransform>().localPosition = startPos;
        //if you have touched the description trigger with the button, display description
        if (hitDescTrigger)
        {
            //Call the delegate method for changing the description, within the script for the description behavior
            if(OnDescriptionChange != null)
            {
                if (description != null)
                    OnDescriptionChange(description, GetComponent<CollectionButton>());
            }
        }
        isBeingDragged = false;

    }

    private void SetDraggedPosition(PointerEventData eventData)
    {
        if(eventData.pointerEnter != null && eventData.pointerEnter.transform as RectTransform != null)
        {
            Vector3 fingerPos;
            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(draggableArea, eventData.position, eventData.pressEventCamera, out fingerPos))
            {
                //minPosition = draggableArea.rect.min;
                //maxPosition = draggableArea.rect.max;
                
                fingerPos.y = Mathf.Clamp(fingerPos.y, minY, maxY);
                fingerPos.x = Mathf.Clamp(fingerPos.x, minX, maxX);
                fingerPos.z = 0.0f;
                //if(fingerPos.x > minX && fingerPos.x < maxX)
                //{
                 //   if(fingerPos.y > minY && fingerPos.y < maxY)
                        gameObject.GetComponent<RectTransform>().localPosition = fingerPos;
                    
                //}
                
            }
        }
    }
	

	// Update is called once per frame
	void Update () {

        if (!isUnlockedShamisen && !isUnlockedShinobue && unlockedKyoImage != null && unlockedKyoImage2 != null && lockedKyoText != null)
        {
            unlockedKyoImage.enabled = false;
            unlockedKyoImage2.enabled = false;
            lockedKyoText.text = "?";
        }
        //check to see if a specific cateegory is unlocked
        switch (instrumentType)
        {
            case CategoryChangeButton.InstrumentCategory.shamisen:
                if (isUnlockedShamisen && unlockedKyoImage != null && unlockedKyoImage2 != null && lockedKyoText != null)
                {
                    unlockedKyoImage.enabled = true;
                    unlockedKyoImage2.enabled = false;
                    lockedKyoText.text = "";
                }
                break;
            case CategoryChangeButton.InstrumentCategory.shinobue:
                if (isUnlockedShinobue && unlockedKyoImage != null && unlockedKyoImage2 != null && lockedKyoText != null)
                {
                    unlockedKyoImage.enabled = false;
                    unlockedKyoImage2.enabled = true;
                    lockedKyoText.text = "";
                }
                break;

        }

    }

    public void PlayReward()
    {
        switch (instrumentType)
        {
            case CategoryChangeButton.InstrumentCategory.shamisen:
                PlayShamisen();
                break;
            case CategoryChangeButton.InstrumentCategory.shinobue:
                PlayShinobue();
                break;
        }
    }

    //Plays the clip with the shamisen attached
    public void PlayShamisen()
    {
        if(instrumentType == CategoryChangeButton.InstrumentCategory.shamisen)
        {
            shamisenSource = GetComponent<AudioSource>();
            shamisenSource.clip = rewardClip1;
            shamisenSource.loop = false;
            shamisenSource.Play();
        }
    }
    //Plays the clip with the shinobue attached
    public void PlayShinobue()
    {
        if (instrumentType == CategoryChangeButton.InstrumentCategory.shinobue)
        {
            shamisenSource = GetComponent<AudioSource>();
            shamisenSource.clip = rewardClip2;
            shinobueSource.loop = false;
            shinobueSource.Play();
        }
    }

    public void ChangeInstrumentCategory(CategoryChangeButton.InstrumentCategory newCategory)
    {
        prevButtonNumber = buttonNum;

        if(instrumentType == CategoryChangeButton.InstrumentCategory.shamisen && newCategory == CategoryChangeButton.InstrumentCategory.shinobue)
        {
            instrumentType = newCategory;
            buttonNum = prevButtonNumber + 8;
            CheckCurrentCategory();
        }
        else if(instrumentType == CategoryChangeButton.InstrumentCategory.shinobue && newCategory == CategoryChangeButton.InstrumentCategory.shamisen)
        {
            instrumentType = newCategory;
            buttonNum = prevButtonNumber - 8;
            CheckCurrentCategory();
        }

    }
    /// <summary>
    /// This makes no sense, try again tomorrow
    /// </summary>
    public void CheckCurrentCategory()
    {
        switch (instrumentType)
        {
            case CategoryChangeButton.InstrumentCategory.shamisen:
                //Apply all of the different names to the different kyokubo
                switch (buttonNum)
                {
                    case 1:
                        kyoName = "Ai";
                        break;
                    case 2:
                        kyoName = "Hii";
                        break;
                    case 3:
                        kyoName = "Inokoza";
                        break;
                    case 4:
                        kyoName = "Kataaki";
                        break;
                    case 5:
                        kyoName = "Kugmagamine";
                        break;
                    case 6:
                        kyoName = "Ringo";
                        break;
                    case 7:
                        kyoName = "Yushiya";
                        break;
                    case 8:
                        kyoName = "Kunkun";
                        break;
                }
                //check the player data to see if they are unlocked or not
                if (PlayerData.playerData != null)
                {
                    playerCollection = PlayerData.playerData.kyokuboCollection;
                    foreach (Kyokubou kyo in playerCollection)
                    {
                        print(kyo.name);
                        if(kyo != null && kyoName != "")
                        {
                            if (kyo.name == kyoName)
                            {
                                print(kyo.reward);
                                rewardStr = kyo.reward;
                                isUnlockedShamisen = true;
                            }
                        }
                        
                    }
                }

                break;

            case CategoryChangeButton.InstrumentCategory.shinobue:
                switch (buttonNum)
                {
                    case 9:
                        kyoName = "Kokoko";
                        break;
                    case 10:
                        kyoName = "Komiya";
                        break;
                    case 11:
                        kyoName = "Osachi";
                        break;
                    case 12:
                        kyoName = "Shouji";
                        break;
                    case 13:
                        kyoName = "Tega";
                        break;
                    case 14:
                        kyoName = "Tomaru";
                        break;
                    case 15:
                        kyoName = "Ushikyuu";
                        break;
                    case 16:
                        kyoName = "Fuwari";
                        break;
                }

                if (PlayerData.playerData != null)
                {
                    playerCollection = PlayerData.playerData.kyokuboCollection;
                    foreach (Kyokubou kyo in playerCollection)
                    {
                        if (kyo != null && kyoName != "")
                        {
                            if (kyo.name == kyoName)
                            {
                                rewardStr = kyo.reward;
                                isUnlockedShinobue = true;
                            }
                        }
                    }
                }
                break;
        }
        
    }
}
