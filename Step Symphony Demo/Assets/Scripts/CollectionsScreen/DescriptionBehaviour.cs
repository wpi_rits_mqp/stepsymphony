﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class DescriptionBehaviour : MonoBehaviour {

    GameObject[] collectionButtons;
    Text descriptionText;
    Dictionary<int, string> descriptionDictionary;
	// Use this for initialization
	void Awake ()
    {
        descriptionDictionary = new Dictionary<int, string>();
        descriptionText = GetComponent<Text>();
        collectionButtons = GameObject.FindGameObjectsWithTag("CollectionButton");
       

        if (collectionButtons.Length > 0)
        {
            foreach(GameObject collectionButton in collectionButtons)
            {
                descriptionDictionary.Add(collectionButton.GetComponent<CollectionButton>().buttonNum, collectionButton.GetComponent<CollectionButton>().description);
                //print("Added description to the dictionary");
                collectionButton.GetComponent<CollectionButton>().OnDescriptionChange += OnDescriptionChange; //calls delegate to run the description change
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDescriptionChange(string newDescription, CollectionButton buttonPressed)
    {
        string Ai_script = "Ai spends most of her time at Kiyomizu - dera. Every week, she visits the love stones and tries to walk between them with her eyes closed. Every week, she fails. But that's fine. She'll just go play a few tunes, drink to her health from the waterfall, eat some shaved ice with Komiya, and try again next week. Shopping doesn't hurt either.";
        string Hii_script = "Hii and Kokoko have been best friends since they were small. In fact, it was Kokoko's father that taught Hii to play the shamisen. However, Hii's true passion is dancing. If you want to find Hii, there are two places to look. Either she is around Nishiki Market, singing and dancing, or she is with Kokoko.";
        string Inokoza_script = "You may have seen Inokoza at the Kabuki Theater. He's been in many performances there over the past few years, always sitting on stage next to his friend Kataaki. Inokoza lives close by, near the park by Yasaka Shrine. He likes to spend time there, paying for visitors and tourists alike. On rare occasions, you may even see him dance.";
        string Kataaki_script = "Kataaki grew up playing the sanshin in Okinawa. His parents never approved, seeing him as lazy for spending less time helping around the house to play. When he was old enough, he moved to Kyoto and learned to play the shamisen. During Gion Matsuri, Kataaki can be seen near Yasaka Shrine eating yakitori and playing his shamisen with his daughter Kokoko.";
        string Kumagamine_script = "Kumagamine loves taking naps in the trees at Kyoto Imperial Palace after class. It's easy for him to get there as well, as he attends a high school nearby. Kumagamine is learning to sing and play the shamisen, just like his grandfather. If you do happen to find him asleep, please wake him gently. He may play a song for you.";
        string KunKun_script = "Every afternoon, Kun-Kun goes to visit Honnoji Temple. While there, he feeds the fish, then sits and waits. Every single day, Kun-Kun sits there until the sun is long set, only to leave the way he came with a sad smile on his face. No one knows why Kun-Kun does this. Is he maybe waiting for someone?";
        string Ringo_script = "Ringo is a big girl now, she just turned six! She loves running around Kyoto Aquarium and Umekoji Park, chasing after Uncle Tega. Tega always gets upset when Ringo catches him, and Ringo doesn't know why. Ringo just started learning to play the shamisen. She's not very good yet, but will get better. Maybe one day she'll get to perform during the dolphin show!";
        string Tomaru_script = "Tomaru loves to spend time in Okazaki park. On some days, she plays her shamisen. On other days, she paints. Some day, Tomaru hopes to have one of her paintings displayed in the Museum of Modern Art. Until then, she'll spend her days sitting in the shade, painting and playing.";
        string Fuwari_script = "Fuwari often accompanies Shouji to Nijo Castle. When he tells his tales to visitors, Fuwari performs these stories by dancing and playing her Shinobue. Sometimes, when the sun is hot in the sky and she needs a change of scenery, Fuwari will take Shouji to visit Osachi at Shosei-en Garden.";
        string Kokoko_script = "As a child, Kokoko loved going to the Minamiza Kabuki Theatre. Her mother would take her every weekend to watch her father, Kataaki, play. Now, Kokoko spends much of her time performing on stage, as well as playing the Shinobue with her father on the streets of Gion.";
        string Komiya_script = "Komiya likes people. People are great. They pet Komiya. take pictures with Komiya. In return, Komiya helps the people. Shows them the best shops walking up to Kiyomizu-dera. Shows them the best places to take pictures. Komiya also spends time with Ai, cheering her on as she tries to reach the other love stone. Komiya is pretty happy.";
        string Osachi_script = "Osachi is always near Nishi and Higashi Honganji Temple. Every morning as the sun rises, he sweeps the front gates for visitors. During the day, he carries messages between the two. When the sun is bright in the sky and the weather is nice, Osachi will visit the Shosei-en Garden with his friend Tega, where they will play their shinobue as they watch the fish swim by.";
        string Shouji_script = "If you want to find Shouji, always look near Nijo Castle. Check the entrance first, he may be welcoming guests in. Check the water next, he may have gone for a swim. When all else fails, check the gardens. That's where Shouji tells his stories. And trust me, Shouji's tales are wonders that you do not want to miss.";
        string Tega_script = "If you know what's good for you, never mention the Kyoto Aquarium in front of Tega. Every day, he tries to escape. Every time, he's always caught in either Umekoji Park or Shosei-en Garden and brought back. Tega doesn't want to be there, he wants to spend time with Osachi. So don't send him back to the Aquarium. He'll never forgive you.";
        string Ushi_script = "Ushikyuu is rarely seen away from Myoshin-ji. If you want to find him, look outdoors. In the mornings, Ushikyuu watches the sun rise. At dusk, he watches the sun set. During the day, look near the temple bell. You'll find him there, playing his shinobue for anyone who cares to listen.";
        string Yushiya_script = "Whenever Ai has questions about the latest fashion trends, she always goes to Yushiya for help. Yushiya works at a clothing store in Kyoto Station, and is always excited to talk about new makeup and mail polish. If you need advice on coordinating outfits, or just want company going to the spa, then never hesitate to ask Yushiya. She will never say no.";


        // if (descriptionDictionary.ContainsKey(buttonPressed.buttonNum))
        // {
        //     descriptionText.text = newDescription;
        //     print("Changed description");
        //     buttonPressed.hitDescTrigger = false;
        //  }

        CategoryChangeButton yes = GameObject.FindGameObjectWithTag("CategoryButton").GetComponentInChildren<CategoryChangeButton>();
        if(buttonPressed.buttonNum == 1)
        {
            if(yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = Ai_script;
            }
            else
            {
                descriptionText.text = Kokoko_script;
            }
            
        }

        else if (buttonPressed.buttonNum == 2)
        {
            if (yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = Hii_script;
            }
            else
            {
                descriptionText.text = Komiya_script;
            }
        }

        else if (buttonPressed.buttonNum == 3)
        {
            if (yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = Inokoza_script;
            }
            else
            {
                descriptionText.text = Osachi_script;
            }
        }
        else if (buttonPressed.buttonNum == 4)
        {
            if (yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = Kataaki_script;
            }
            else
            {
                descriptionText.text = Shouji_script;
            }
        }
        else if (buttonPressed.buttonNum == 5)
        {
            if (yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = Kumagamine_script;
            }
            else
            {
                descriptionText.text = Tega_script;
            }
        }
        else if (buttonPressed.buttonNum == 6)
        {
            if (yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = Ringo_script;
            }
            else
            {
                descriptionText.text = Yushiya_script;
            }
        }
        else if (buttonPressed.buttonNum == 7)
        {
            if (yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = Tomaru_script;
            }
            else
            {
                descriptionText.text = Ushi_script;
            }
        }
        else if (buttonPressed.buttonNum == 8)
        {
            if (yes.categoryText.text == "Shamisen")
            {
                descriptionText.text = KunKun_script;
            }
            else
            {
                descriptionText.text = Fuwari_script;
            }
        }

    }
}
