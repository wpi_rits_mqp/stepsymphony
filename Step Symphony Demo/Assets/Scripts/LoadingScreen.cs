﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    [SerializeField]
    private float m_minDuration = 1.5f;

    public string scene = "";

    private AsyncOperation async = null;

    //Updates once per frame
    public void Awake()
    {
        scene = PlayerPrefs.GetString("SceneToLoad");
        //print("0");
        StartCoroutine(LoadSceneAsync(scene));
    }


    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    IEnumerator LoadSceneAsync(string sceneName)
    {
        //SceneManager.LoadSceneAsync("LoadingScreen");

        //System.Threading.Thread.Sleep(1000);

        SceneManager.LoadSceneAsync(sceneName);
        yield return async;
        //int loopCount = 0;
        //while (true)
        //{
        //    loopCount++;
        //    if (null != GameObject.FindGameObjectWithTag("Player"))
        //    {
        //        break;
        //    }

        //    if (500 == loopCount)
        //    {
        //        Debug.LogWarning("Waited 500 fixed updates for level geometry to appear. Aborting.");
        //        yield break;
        //    }
        //    yield return new WaitForFixedUpdate();


            /*//Load loading screen
            SceneManager.LoadSceneAsync("LoadingScreen");

            AsyncOperation loadLevel = SceneManager.LoadSceneAsync(sceneName);

            //loadLevel.allowSceneActivation = false;

                // Wait until done and collect progress as we go.
                while (!loadLevel.isDone)
                {
                    if (loadLevel.progress >= 0.9f)
                    {
                        // Almost done.
                        break;
                    }
                }

            // Allow new scene to start.
            //loadLevel.allowSceneActivation = true;
            // !!! unload loading screen
            LoadingSceneManager.UnloadLoadingScene();
            yield return loadLevel;


        */
        //}
    }

}
