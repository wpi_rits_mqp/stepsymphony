﻿using UnityEngine;
using System.Collections;

namespace Assets.Models
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Boundary : MonoBehaviour
    {
        public string Id;
        public string Kind;
        public string Type;
        public string Name;
        public int SortKey;
    }
}
