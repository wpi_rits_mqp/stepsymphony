﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Models.Enums
{
    public enum WaterType
    {
        Unknown,
        Basin,
        Dock,
        Lake,
        Ocean,
        Playa,
        Riverbank,
        Swimming_Pool,
        Water,
    }
}
