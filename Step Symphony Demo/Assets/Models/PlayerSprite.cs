﻿using System;
using System.Collections.Generic;
using Assets;
using Assets.Helpers;
using Assets.Models;
using UnityEngine;
using System.Collections;
using Assets.Models.Factories;

public class PlayerSprite : MonoBehaviour
{
    GameObject player;
    double player_lat, player_long;
    double center_lat, center_long;

    Vector2 player_loc_meters; //players location in x/z and coordinates
    Vector2 tile_center_meters;
    Vector3 tile_center_XZ; //tile center location in x/z and coordinates;
    Vector3 difference;  //difference between two locations in x/z and coorinates
    Vector3 new_position; //The player's new position to lerp to

    int count;
    Vector2 conversions;
    GameObject World;
    TileManager worldScript;
    bool alreadyDid;

    Tile tile;

    public void Start()
    {
        Input.location.Start();

        //Set Player
        player = GameObject.FindGameObjectWithTag("Player");

        //Set FrameRate and counters
        Application.targetFrameRate = 60;
        count = 0;
        alreadyDid = false;
        new_position = new Vector3(0, 11, 0);

        //Find the TileManager Script
        World = GameObject.Find("World");
        worldScript = World.GetComponent<TileManager>();

        center_lat = worldScript.tile_lat + .001;
        center_long = worldScript.tile_long + .001;

        player_lat = center_lat;
        player_long = center_long;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //If there is a Player
        if (player != null)
        {  
            //Set the tile center's XYZ coordinates to the current Player location
            
            if (count >= 300)
            {
                if (!alreadyDid) //If tile_center_meters hasn't been set once yet
                {
                    tile_center_meters.x = (float) worldScript.CenterInMercator.x;
                    tile_center_meters.y = (float)worldScript.CenterInMercator.y;//Set it to current tile's center in meters
                    alreadyDid = true; //And don't do this afain
                }
                tile_center_XZ = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
                new_position = tile_center_XZ;

               // player_lat = player_lat - .00001;
               // player_long = player_long - .00001;

                player_lat = Input.location.lastData.latitude;
                player_long = Input.location.lastData.longitude;



                StartCoroutine(movePlayer());
                count = count - 60;
            }       
        }
             count++;
    }
    
    public IEnumerator movePlayer()
    {
        //The players lat and longitude
        player_loc_meters = new Vector2((float) player_lat,  (float) player_long);
        player_loc_meters = GM.LatLonToMeters2(player_loc_meters);  //Convert the lat and long into meters

        difference = new Vector3(player_loc_meters.x - tile_center_meters.x, 0, player_loc_meters.y - tile_center_meters.y);
        difference = new Vector3(difference.x * World.transform.lossyScale.x, 0, difference.z * World.transform.lossyScale.z);

        new_position = new_position + difference;

        player.transform.position = Vector3.Lerp(tile_center_XZ, new_position, 1);

        tile_center_meters = player_loc_meters;
        yield return null;
    }

}
