﻿using UnityEngine;
using System.Collections;

public class CompassController : MonoBehaviour {
    //initial variables 
    public WebCamTexture webcamTexture = null;
    public Quaternion baseRotation;
    public int scr_height = Screen.height;
    public int scr_width = Screen.width;

    //variables for finding the center of the shere
    public Vector3 axis = Vector3.up;
    public Vector3 des_pos;
    public float rad = 18.0f;
    public float rad_speed = 1.5f;
    public float rot_speed = 65f;
    public float player_compass_angle;
    public GameObject center_obj;
    public GameObject display_screen;
    public Transform center;

    //Direction Enumeration
    public enum Direction { north = 1, south = 2, east = 3, west = 4, northeast = 5, northwest = 6, southeast = 7, southwest = 8 };
    public Direction curr_direction = 0;

    // Use this for initialization
    void Start () {
        //initialize the center of the game object
        center_obj = GameObject.FindGameObjectWithTag("Center");
        display_screen = GameObject.FindGameObjectWithTag("Display");
        center = center_obj.transform;

        transform.position = (transform.position - center.position).normalized * rad + center.position;
        rad = display_screen.transform.position.z - center.position.z; //distance between the center object and the display screen

        //Start location services if the player enables them
        if (Input.location.isEnabledByUser)
        {
            Input.location.Start();
            Input.compass.enabled = true;


        }
        webcamTexture = new WebCamTexture(scr_width, scr_height);
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webcamTexture;
        webcamTexture.Play();
	}
	
	// Update is called once per frame
	void Update () {
        //Gets the compass input of the mobile device every frame
        player_compass_angle = Input.compass.trueHeading;
        findPlayerDir(player_compass_angle);
        //make a 8 case state based off of the direction the player is facing
        switch (curr_direction)
        {
            case Direction.east:
                //Rotate around the center axis to the right
                transform.RotateAround(center.position, axis, -rot_speed * Time.deltaTime);

                break;
            case Direction.west:
                //Rotate around the center axis to the left
                transform.RotateAround(center.position, axis, rot_speed * Time.deltaTime);
                break;
            default:
                break;
        }
         
	}
    /**
        This function sets a boolean flag for the direction the phone compass is facing.
        All will return false if a compass angle cannot be determined.
    **/
    public Direction findPlayerDir(float player_angle)
    {
        if (player_angle > 85f && player_angle < 95f)
        { //East between 85 degrees and 95 degrees
           curr_direction = Direction.east;
           return curr_direction;
        }
        else if (player_angle > 265f && player_angle < 275f)
        { //West between 265 degrees and 275 degrees
            curr_direction = Direction.west;
            return curr_direction;
        }
        else if ((player_angle < 5f && player_angle >= 0f) || (player_angle <= 360f && player_angle > 355f))
        { //South either less than 5 and greater than or equal to 0 or less than 360 and greater than 355
            curr_direction = Direction.south;
            return curr_direction;
        }
        else if (player_angle < 185f && player_angle > 175f)
        { //North either less than 185 degrees and greater than 175 degrees
            curr_direction = Direction.north;
            return curr_direction;
        }
        else if (player_angle < 50f && player_angle > 40f)
        { //SouthEast either less than 50 degrees and greater than 40 degrees
            curr_direction = Direction.southeast;
            return curr_direction;
        }
        else if (player_angle < 140f && player_angle > 130f)
        { //NorthEast either less than 140 degrees and greater than 130 degrees
            curr_direction = Direction.northeast;
            return curr_direction;
        }
        else if (player_angle < 320f && player_angle > 310f)
        { //SouthWest either less than 320 degrees and greater than 310 degrees
            curr_direction = Direction.southwest;
            return curr_direction;
        }
        else if (player_angle < 230f && player_angle > 220f)
        { //NorthWest either less than 230 degrees and greater than 220 degrees
            curr_direction = Direction.northwest;
            return curr_direction;
        }
        else { //None
            curr_direction = 0;
            return curr_direction;
        }
    }
}
