﻿using UnityEngine;
using System.Collections;

public class MoveCamera_Accelerometer : MonoBehaviour {
    //initial variables 
    public WebCamTexture webcamTexture = null;
    public Quaternion baseRotation;
    public int scr_height;
    public int scr_width;

    //variables for finding the center of the sphere
    public Vector3 axis = Vector3.up;
    public Vector3 des_pos;
    public float rad;//find distance between camera and display object
    public float rad_speed = 1.5f;
    public float rot_speed = 65f;
    public float player_compass_angle;
    public GameObject center_obj;
    public GameObject display_screen;
    public Transform center;

    //private Vector3 accelerometer;


    // Use this for initialization
    void Start () {
       // accelerometer = Input.acceleration;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //Test to see if moving the phone about the x axis 
       
        print(Input.acceleration.y);
	}
}
