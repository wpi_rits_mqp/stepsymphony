﻿using UnityEngine;
using System.Collections;

public class Overlay_BackCamTexture : MonoBehaviour {
    public WebCamTexture webCamTexture;


    // Use this for initialization
    void Start () {
        webCamTexture = new WebCamTexture();
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webCamTexture;
        if(webCamTexture != null)
            webCamTexture.Play();
	}
	
	// Update is called once per frame
	void Update () {
        
	}
}
