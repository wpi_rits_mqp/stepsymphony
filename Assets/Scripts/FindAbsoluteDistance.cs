﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FindAbsoluteDistance : MonoBehaviour 
{
    public GameObject target_obj;
    public Transform target_transform;
    
    public float abs_distance;
    public bool facing_camera;

    public Text dist_display;
    private Ray ray;
    private RaycastHit hit;
    private Vector3 gyro_rotation;

	// Use this for initialization
	void Start ()
    {
        //find the target object
        target_obj = GameObject.FindGameObjectWithTag("Target");
        target_transform = target_obj.transform;

        //find the text object to edit, located within the target object
        dist_display = target_obj.GetComponentInChildren<Text>();

	}
	
	// Update is called once per frame
	void Update ()
    {
        //Check if the camera is facing the target object
       // if(CheckFacingPlayer() == true)
      //  {
            //Write the absolute distance as a string and set the text object
            abs_distance = AbsDistance();
            dist_display.text = abs_distance.ToString();
            
       // }


    }

    //Arguments: 
    bool CheckFacingPlayer()
    {
        gyro_rotation = new Vector3(Input.gyro.attitude.x, Input.gyro.attitude.y, Input.gyro.attitude.z);
        ray = Camera.main.ScreenPointToRay(gyro_rotation);
        if (Physics.Raycast(ray, out hit) == true && hit.transform.tag == "Target")
        {
            facing_camera = true;
            return facing_camera;
        }
        else
        {
            facing_camera = false;
            return facing_camera;
        }
            
    }

    //Find the absolute value of the distance between the player and the target object
    //Returns a float
    float AbsDistance()
    {

        return Mathf.Abs(Vector3.Distance(transform.position, target_transform.position));
    }
}
