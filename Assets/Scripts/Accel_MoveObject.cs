﻿using UnityEngine;
using System.Collections;


public class Accel_MoveObject : MonoBehaviour 
{
    //public Transform init_transform;
    public GameObject cam_target;
    public Vector2 cam_rot;
    public Quaternion rot_fix;
    bool gyro_enabled;
    

    // Use this for initialization
    void Start ()
    {
        //init_transform = transform;
        Input.gyro.enabled = true;
        gyro_enabled = true;
        cam_target = GameObject.FindGameObjectWithTag("CamTarget");
        cam_target.transform.Rotate(Vector3.right, 90);



    }
	
	// Update is called once per frame
	void Update ()
    {
        //Check to see if the gyro is enabled
        if (gyro_enabled)
        {
            
            //Invert the z and w of the gyro attitude
            rot_fix = new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);

            //change the local translation to be the child camera object
            transform.localRotation = rot_fix;
        }
            
    }


}
