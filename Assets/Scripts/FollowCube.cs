﻿using UnityEngine;
using System.Collections;

public class FollowCube : MonoBehaviour {
    public GameObject target;
    public float x_offset = 0f;
    public float y_offset = 0f;
    public float z_offset = 0f;

	
	
	// Update is called once per frame
	void LateUpdate () {
        this.transform.position = new Vector3(target.transform.position.x + x_offset,
                                              target.transform.position.y + y_offset,
                                              target.transform.position.z + z_offset);
	}
}
